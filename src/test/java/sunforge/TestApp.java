package sunforge;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.jupiter.api.Test;

import static sunforge.dao.ImportService.initializeData;

public class TestApp {
    @Test
    public void setupDB() {
        Properties properties = new Properties();
        try (InputStream in = new FileInputStream("src/test/resources/db.properties")) {
            properties.load(in);
            initializeData(properties);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}