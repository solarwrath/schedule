package sunforge;

import org.junit.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import sunforge.model.Day;
import sunforge.model.AcademicGroup;
import sunforge.model.Professor;
import sunforge.model.Room;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class TestDBImport {
    @Nested
    public static class TestGroups {
        @Test
        @DisplayName("Groups corrected initialized")
        public void checkGroups() {
            AcademicGroupImportSample groups[] = {
                    new AcademicGroupImportSample(1, "109 тест", Day.MONDAY),
                    new AcademicGroupImportSample(2, "209", Day.THURSDAY)
            };

            Set<AcademicGroup> pool = AcademicGroup.getPool();
            for (AcademicGroupImportSample a : groups) {
                AcademicGroup b = pool.stream().filter(academicGroup ->
                        academicGroup.getID() == a.ID
                ).findFirst().get();

                Assert.assertEquals(a.ID, b.getID());
                Assert.assertEquals(a.name, b.getGroupName());
                Assert.assertEquals(a.excludedDay, b.getExcludedDay());
            }
        }

        private class AcademicGroupImportSample {
            private long ID;
            private String name;
            private Day excludedDay;


            public AcademicGroupImportSample(long ID, String name, Day excludedDay) {
                this.ID = ID;
                this.name = name;
                this.excludedDay = excludedDay;
            }
        }
    }

    @Nested
    public static class TestProfessors{
        @Test
        @DisplayName("Professors initialized")
        public void checkProfessors(){
            Map<Integer, String> testData = new HashMap<>();
            testData.put(1, "Воробйова А. І.");
            testData.put(2, "Варшамов А. В.");
            testData.put(3, "Кірей К. О.");
            testData.put(4, "Голобородько А. М.");
            testData.put(5, "Бойко А. П.");
            testData.put(6, "Фаленкова М. В.");
            testData.put(7, "Нездолій Ю. О.");

            Set<Professor> pool = Professor.getPool();
            pool.forEach(professor -> {
                long id = professor.getID();
                Integer convertedID = new Long(id).intValue();

                Assert.assertEquals(testData.get(convertedID), professor.getName());
            });
        }
    }

    @Nested
    public static class TestRooms3{
        @Test
        @DisplayName("Rooms initialized")
        public void checkRooms(){
            Set<Room> pool = Room.getPool();

            Room room = pool.stream().findFirst().get();
            Assert.assertEquals(1, room.getID());
            Assert.assertEquals("1-300", room.getName());
        }
    }
}