package sunforge.export;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sunforge.model.Day;
import sunforge.model.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ExportService {
    public static void export(File file) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet("Расписание");

        //TODO Adjust for year
        for (int i = 0; i < 125; i++) {
            sheet.createRow(i);
        }

        renderHeaders(sheet);

        int currentRowPos = 4;
        int currentColPos = 2;

        XSSFFont headersFont = workbook.createFont();
        headersFont.setFontHeightInPoints((short) 16);
        headersFont.setFontName("Arial");
        headersFont.setBold(true);

        CellStyle groupNameStyle = workbook.createCellStyle();
        groupNameStyle.setAlignment(HorizontalAlignment.CENTER);
        groupNameStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        groupNameStyle.setFont(headersFont);
        groupNameStyle.setBorderTop(BorderStyle.MEDIUM);
        groupNameStyle.setBorderRight(BorderStyle.MEDIUM);
        groupNameStyle.setBorderBottom(BorderStyle.MEDIUM);
        groupNameStyle.setBorderLeft(BorderStyle.MEDIUM);

        CellStyle roomHeaderStyle = workbook.createCellStyle();
        roomHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
        roomHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        roomHeaderStyle.setFont(headersFont);
        roomHeaderStyle.setRotation((short) 90);
        roomHeaderStyle.setBorderTop(BorderStyle.MEDIUM);
        roomHeaderStyle.setBorderRight(BorderStyle.MEDIUM);
        roomHeaderStyle.setBorderBottom(BorderStyle.MEDIUM);
        roomHeaderStyle.setBorderLeft(BorderStyle.MEDIUM);

        List<AcademicGroup> groups = new ArrayList<>(AcademicGroup.getPool());
        groups.sort(new Comparator<AcademicGroup>() {
            @Override
            public int compare(AcademicGroup academicGroup1, AcademicGroup academicGroup2) {
                return academicGroup1.getGroupName().compareTo(academicGroup2.getGroupName());
            }
        });

        for (AcademicGroup currentAcademicGroup : groups) {
            XSSFCell groupName = sheet.getRow(currentRowPos).createCell(currentColPos);
            groupName.setCellValue(currentAcademicGroup.getGroupName());
            groupName.setCellStyle(groupNameStyle);
            sheet.getRow(currentRowPos).createCell(currentColPos + 1).setCellStyle(groupNameStyle);

            sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos, currentColPos, currentColPos + 1));

            XSSFCell roomLabel = sheet.getRow(currentRowPos).createCell(currentColPos + 2);
            roomLabel.setCellValue("Ауд.");
            roomLabel.setCellStyle(roomHeaderStyle);

            currentRowPos++;

            CellStyle classStyle = workbook.createCellStyle();
            classStyle.setWrapText(true);
            classStyle.setAlignment(HorizontalAlignment.CENTER);
            classStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            for (Day currentDay : Day.values()) {
                try {
                    if(currentDay == currentAcademicGroup.getExcludedDay()){
                        throw new SpecifiedDayIsExcluded(currentDay);
                    }

                    List<AcademicClass> dayClasses = new ArrayList<>();
                    dayClasses.addAll(currentAcademicGroup.getAssignedClasses().get(Week.FIRST).get(currentDay));
                    dayClasses.addAll(currentAcademicGroup.getAssignedClasses().get(Week.SECOND).get(currentDay));
                    //TODO Change mb for lower years
                    for (int idxInDay = 0; idxInDay < 6; idxInDay++) {
                        List<AcademicClass> currentPairOfClasses = new ArrayList<>();
                        int finalIdxInDay = idxInDay;
                        dayClasses.forEach(academicClass -> {
                            if(academicClass.getIndexInDay() == finalIdxInDay){
                                currentPairOfClasses.add(academicClass);
                            }
                        });

                        int pairSize = currentPairOfClasses.size();
                        if (pairSize == 0) {
                            XSSFCell classCell = sheet.getRow(currentRowPos).createCell(currentColPos);
                            classCell.setCellStyle(classStyle);

                            sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos, currentColPos + 1));
                            sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos + 2, currentColPos + 2));

                            CellRangeAddress classRegion = new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos, currentColPos + 1);
                            RegionUtil.setBorderBottom(BorderStyle.MEDIUM, classRegion, sheet);
                            RegionUtil.setBorderTop(BorderStyle.MEDIUM, classRegion, sheet);
                            RegionUtil.setBorderLeft(BorderStyle.MEDIUM, classRegion, sheet);
                            RegionUtil.setBorderRight(BorderStyle.MEDIUM, classRegion, sheet);

                            CellRangeAddress roomRegion = new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos + 2, currentColPos + 2);
                            RegionUtil.setBorderBottom(BorderStyle.MEDIUM, roomRegion, sheet);
                            RegionUtil.setBorderTop(BorderStyle.MEDIUM, roomRegion, sheet);
                            RegionUtil.setBorderLeft(BorderStyle.MEDIUM, roomRegion, sheet);
                            RegionUtil.setBorderRight(BorderStyle.MEDIUM, roomRegion, sheet);
                        } else if (pairSize == 1) {
                            AcademicClass academicClass = currentPairOfClasses.get(0);
                            Week week = academicClass.getAssignedWeek();
                            XSSFCell classCell;
                            if (week == Week.FIRST) {
                                classCell = sheet.getRow(currentRowPos).createCell(currentColPos);
                            } else {
                                classCell = sheet.getRow(currentRowPos + 2).createCell(currentColPos);
                            }

                            classCell.setCellStyle(classStyle);
                            classCell.setCellValue(academicClass.getSubjectName() + "\n" + academicClass.getProfessor().getName());
                            if (academicClass.getAssignedRoom() != null) {
                                XSSFCell roomCell;
                                if (week == Week.FIRST) {
                                    roomCell = sheet.getRow(currentRowPos).createCell(currentColPos + 2);
                                } else {
                                    roomCell = sheet.getRow(currentRowPos + 2).createCell(currentColPos + 2);
                                }
                                roomCell.setCellValue(academicClass.getAssignedRoom().getName());
                            }

                            sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos + 1, currentColPos, currentColPos + 1));
                            sheet.addMergedRegion(new CellRangeAddress(currentRowPos + 2, currentRowPos + 3, currentColPos, currentColPos + 1));
                            sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos + 1, currentColPos + 2, currentColPos + 2));
                            sheet.addMergedRegion(new CellRangeAddress(currentRowPos + 2, currentRowPos + 3, currentColPos + 2, currentColPos + 2));

                            CellRangeAddress firstWeekClassRegion = new CellRangeAddress(currentRowPos, currentRowPos + 1, currentColPos, currentColPos + 1);
                            RegionUtil.setBorderBottom(BorderStyle.MEDIUM, firstWeekClassRegion, sheet);
                            RegionUtil.setBorderTop(BorderStyle.MEDIUM, firstWeekClassRegion, sheet);
                            RegionUtil.setBorderLeft(BorderStyle.MEDIUM, firstWeekClassRegion, sheet);
                            RegionUtil.setBorderRight(BorderStyle.MEDIUM, firstWeekClassRegion, sheet);

                            CellRangeAddress secondWeekClassRegion = new CellRangeAddress(currentRowPos + 2, currentRowPos + 3, currentColPos, currentColPos + 1);
                            RegionUtil.setBorderBottom(BorderStyle.MEDIUM, secondWeekClassRegion, sheet);
                            RegionUtil.setBorderTop(BorderStyle.MEDIUM, secondWeekClassRegion, sheet);
                            RegionUtil.setBorderLeft(BorderStyle.MEDIUM, secondWeekClassRegion, sheet);
                            RegionUtil.setBorderRight(BorderStyle.MEDIUM, secondWeekClassRegion, sheet);

                            CellRangeAddress firstWeekRoomRegion = new CellRangeAddress(currentRowPos, currentRowPos + 1, currentColPos + 2, currentColPos + 2);
                            RegionUtil.setBorderBottom(BorderStyle.MEDIUM, firstWeekRoomRegion, sheet);
                            RegionUtil.setBorderTop(BorderStyle.MEDIUM, firstWeekRoomRegion, sheet);
                            RegionUtil.setBorderLeft(BorderStyle.MEDIUM, firstWeekRoomRegion, sheet);
                            RegionUtil.setBorderRight(BorderStyle.MEDIUM, firstWeekRoomRegion, sheet);

                            CellRangeAddress secondWeekRegion = new CellRangeAddress(currentRowPos + 2, currentRowPos + 3, currentColPos + 2, currentColPos + 2);
                            RegionUtil.setBorderBottom(BorderStyle.MEDIUM, secondWeekRegion, sheet);
                            RegionUtil.setBorderTop(BorderStyle.MEDIUM, secondWeekRegion, sheet);
                            RegionUtil.setBorderLeft(BorderStyle.MEDIUM, secondWeekRegion, sheet);
                            RegionUtil.setBorderRight(BorderStyle.MEDIUM, secondWeekRegion, sheet);
                        } else if (pairSize == 2) {
                            Week weekOfFirstClass = currentPairOfClasses.get(0).getAssignedWeek();
                            AcademicClass firstWeekClass;
                            AcademicClass secondWeekClass;

                            if (weekOfFirstClass == Week.FIRST) {
                                firstWeekClass = currentPairOfClasses.get(0);
                                secondWeekClass = currentPairOfClasses.get(1);
                            } else {
                                firstWeekClass = currentPairOfClasses.get(1);
                                secondWeekClass = currentPairOfClasses.get(0);
                            }

                            if (firstWeekClass.getAssignedRoom() == secondWeekClass.getAssignedRoom() &&
                                    firstWeekClass.getProfessor() == secondWeekClass.getProfessor() &&
                                    firstWeekClass.getType() == secondWeekClass.getType() &&
                                    firstWeekClass.getSubjectName() == secondWeekClass.getSubjectName() &&
                                    firstWeekClass.getAdditionalRequirements() == secondWeekClass.getAdditionalRequirements()) {

                                XSSFCell classCell = sheet.getRow(currentRowPos).createCell(currentColPos);
                                classCell.setCellStyle(classStyle);
                                classCell.setCellValue(firstWeekClass.getSubjectName() + "\n" + firstWeekClass.getProfessor().getName());

                                if (firstWeekClass.getAssignedRoom() != null) {
                                    XSSFCell roomCell = sheet.getRow(currentRowPos).createCell(currentColPos + 2);
                                    roomCell.setCellValue(firstWeekClass.getAssignedRoom().getName());
                                }

                                CellRangeAddress classRegion = new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos, currentColPos + 1);
                                RegionUtil.setBorderBottom(BorderStyle.MEDIUM, classRegion, sheet);
                                RegionUtil.setBorderTop(BorderStyle.MEDIUM, classRegion, sheet);
                                RegionUtil.setBorderLeft(BorderStyle.MEDIUM, classRegion, sheet);
                                RegionUtil.setBorderRight(BorderStyle.MEDIUM, classRegion, sheet);

                                CellRangeAddress roomRegion = new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos + 2, currentColPos + 2);
                                RegionUtil.setBorderBottom(BorderStyle.MEDIUM, roomRegion, sheet);
                                RegionUtil.setBorderTop(BorderStyle.MEDIUM, roomRegion, sheet);
                                RegionUtil.setBorderLeft(BorderStyle.MEDIUM, roomRegion, sheet);
                                RegionUtil.setBorderRight(BorderStyle.MEDIUM, roomRegion, sheet);
                            } else {
                                XSSFCell firstClassCell = sheet.getRow(currentRowPos).createCell(currentColPos);
                                firstClassCell.setCellStyle(classStyle);
                                firstClassCell.setCellValue(firstWeekClass.getSubjectName() + "\n" + firstWeekClass.getProfessor().getName());
                                if (firstWeekClass.getAssignedRoom() != null) {
                                    XSSFCell roomCell = sheet.getRow(currentRowPos).createCell(currentColPos + 2);
                                    roomCell.setCellValue(firstWeekClass.getAssignedRoom().getName());
                                }

                                sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos + 1, currentColPos, currentColPos + 1));
                                sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos + 1, currentColPos + 2, currentColPos + 2));

                                CellRangeAddress classRegion = new CellRangeAddress(currentRowPos, currentRowPos + 1, currentColPos, currentColPos + 1);
                                RegionUtil.setBorderBottom(BorderStyle.MEDIUM, classRegion, sheet);
                                RegionUtil.setBorderTop(BorderStyle.MEDIUM, classRegion, sheet);
                                RegionUtil.setBorderLeft(BorderStyle.MEDIUM, classRegion, sheet);
                                RegionUtil.setBorderRight(BorderStyle.MEDIUM, classRegion, sheet);

                                CellRangeAddress roomRegion = new CellRangeAddress(currentRowPos, currentRowPos + 1, currentColPos + 2, currentColPos + 2);
                                RegionUtil.setBorderBottom(BorderStyle.MEDIUM, roomRegion, sheet);
                                RegionUtil.setBorderTop(BorderStyle.MEDIUM, roomRegion, sheet);
                                RegionUtil.setBorderLeft(BorderStyle.MEDIUM, roomRegion, sheet);
                                RegionUtil.setBorderRight(BorderStyle.MEDIUM, roomRegion, sheet);

                                XSSFCell secondClassCell = sheet.getRow(currentRowPos + 2).createCell(currentColPos);
                                secondClassCell.setCellStyle(classStyle);
                                secondClassCell.setCellValue(secondWeekClass.getSubjectName() + "\n" + secondWeekClass.getProfessor().getName());
                                if (secondWeekClass.getAssignedRoom() != null) {
                                    XSSFCell roomCell = sheet.getRow(currentRowPos + 2).createCell(currentColPos + 2);
                                    roomCell.setCellValue(secondWeekClass.getAssignedRoom().getName());
                                }

                                sheet.addMergedRegion(new CellRangeAddress(currentRowPos + 2, currentRowPos + 3, currentColPos, currentColPos + 1));
                                sheet.addMergedRegion(new CellRangeAddress(currentRowPos + 2, currentRowPos + 3, currentColPos + 2, currentColPos + 2));

                                CellRangeAddress secondClassRegion = new CellRangeAddress(currentRowPos + 2, currentRowPos + 3, currentColPos, currentColPos + 1);
                                RegionUtil.setBorderBottom(BorderStyle.MEDIUM, secondClassRegion, sheet);
                                RegionUtil.setBorderTop(BorderStyle.MEDIUM, secondClassRegion, sheet);
                                RegionUtil.setBorderLeft(BorderStyle.MEDIUM, secondClassRegion, sheet);
                                RegionUtil.setBorderRight(BorderStyle.MEDIUM, secondClassRegion, sheet);

                                CellRangeAddress secondRoomRegion = new CellRangeAddress(currentRowPos + 2, currentRowPos + 3, currentColPos + 2, currentColPos + 2);
                                RegionUtil.setBorderBottom(BorderStyle.MEDIUM, secondRoomRegion, sheet);
                                RegionUtil.setBorderTop(BorderStyle.MEDIUM, secondRoomRegion, sheet);
                                RegionUtil.setBorderLeft(BorderStyle.MEDIUM, secondRoomRegion, sheet);
                                RegionUtil.setBorderRight(BorderStyle.MEDIUM, secondRoomRegion, sheet);
                            }
                        }

                        currentRowPos += 4;
                    }
                } catch (SpecifiedDayIsExcluded specifiedDayIsExcluded) {
                    for (int i = 0; i < 6; i++) {
                        sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos, currentColPos + 1));
                        sheet.addMergedRegion(new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos + 2, currentColPos + 2));

                        CellRangeAddress classRegion = new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos, currentColPos + 1);
                        RegionUtil.setBorderBottom(BorderStyle.MEDIUM, classRegion, sheet);
                        RegionUtil.setBorderTop(BorderStyle.MEDIUM, classRegion, sheet);
                        RegionUtil.setBorderLeft(BorderStyle.MEDIUM, classRegion, sheet);
                        RegionUtil.setBorderRight(BorderStyle.MEDIUM, classRegion, sheet);

                        CellRangeAddress roomRegion = new CellRangeAddress(currentRowPos, currentRowPos + 3, currentColPos + 2, currentColPos + 2);
                        RegionUtil.setBorderBottom(BorderStyle.MEDIUM, roomRegion, sheet);
                        RegionUtil.setBorderTop(BorderStyle.MEDIUM, roomRegion, sheet);
                        RegionUtil.setBorderLeft(BorderStyle.MEDIUM, roomRegion, sheet);
                        RegionUtil.setBorderRight(BorderStyle.MEDIUM, roomRegion, sheet);

                        currentRowPos += 4;
                    }
                }

            }
            currentColPos += 3;
            currentRowPos = 4;
        }


        try (FileOutputStream out = new FileOutputStream(file)) {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void renderHeaders(XSSFSheet sheet) {
        XSSFFont headersFont = sheet.getWorkbook().createFont();
        headersFont.setFontHeightInPoints((short) 16);
        headersFont.setFontName("Arial");
        headersFont.setBold(true);

        CellStyle groupNameStyle = sheet.getWorkbook().createCellStyle();
        groupNameStyle.setAlignment(HorizontalAlignment.CENTER);
        groupNameStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        groupNameStyle.setFont(headersFont);
        groupNameStyle.setBorderTop(BorderStyle.MEDIUM);
        groupNameStyle.setBorderRight(BorderStyle.MEDIUM);
        groupNameStyle.setBorderBottom(BorderStyle.MEDIUM);
        groupNameStyle.setBorderLeft(BorderStyle.MEDIUM);

        XSSFCell dayLabelCell = sheet.getRow(4).createCell(0);
        dayLabelCell.setCellValue("День");
        dayLabelCell.setCellStyle(groupNameStyle);

        XSSFCell numberLabelCell = sheet.getRow(4).createCell(1);
        numberLabelCell.setCellValue("Пари");
        numberLabelCell.setCellStyle(groupNameStyle);

        XSSFFont biggerHeaderFont = sheet.getWorkbook().createFont();
        biggerHeaderFont.setFontHeightInPoints((short) 18);
        biggerHeaderFont.setFontName("Arial");
        biggerHeaderFont.setBold(true);

        CellStyle dayLabelStyle = sheet.getWorkbook().createCellStyle();
        dayLabelStyle.setAlignment(HorizontalAlignment.CENTER);
        dayLabelStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        dayLabelStyle.setFont(biggerHeaderFont);
        dayLabelStyle.setRotation((short) 90);

        CellStyle numberLabelStyle = sheet.getWorkbook().createCellStyle();
        numberLabelStyle.setAlignment(HorizontalAlignment.CENTER);
        numberLabelStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        numberLabelStyle.setFont(biggerHeaderFont);

        Day dayArr[] = Day.values();
        int currentRow = 5;
        for (Day currentDay : dayArr) {
            XSSFCell dayCell = sheet.getRow(currentRow).createCell(0);
            dayCell.setCellValue(currentDay.toString());
            sheet.addMergedRegion(new CellRangeAddress(currentRow, currentRow + 23, 0, 0));

            dayCell.setCellStyle(dayLabelStyle);

            //TODO Change for lower years
            for (int i = 0; i < 6; i++) {
                XSSFCell numberCell = sheet.getRow(currentRow + i * 4).createCell(1);
                numberCell.setCellValue(i + 1);
                sheet.addMergedRegion(new CellRangeAddress(currentRow + i * 4, currentRow + i * 4 + 3, 1, 1));

                numberCell.setCellStyle(numberLabelStyle);

                CellRangeAddress numberRegion = new CellRangeAddress(currentRow + i * 4, currentRow + i * 4 + 3, 1, 1);
                RegionUtil.setBorderBottom(BorderStyle.MEDIUM, numberRegion, sheet);
                RegionUtil.setBorderTop(BorderStyle.MEDIUM, numberRegion, sheet);
                RegionUtil.setBorderLeft(BorderStyle.MEDIUM, numberRegion, sheet);
                RegionUtil.setBorderRight(BorderStyle.MEDIUM, numberRegion, sheet);

            }

            CellRangeAddress dayRegion = new CellRangeAddress(currentRow, currentRow + 23, 0, 0);
            RegionUtil.setBorderBottom(BorderStyle.MEDIUM, dayRegion, sheet);
            RegionUtil.setBorderTop(BorderStyle.MEDIUM, dayRegion, sheet);
            RegionUtil.setBorderLeft(BorderStyle.MEDIUM, dayRegion, sheet);
            RegionUtil.setBorderRight(BorderStyle.MEDIUM, dayRegion, sheet);

            currentRow += 24;
        }
    }
}