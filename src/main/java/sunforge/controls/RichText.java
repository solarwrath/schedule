package sunforge.controls;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import org.fxmisc.richtext.InlineCssTextArea;

/*
    WHEN USING THIS YOU NEED TO USE INLINE CSS FOR TEXT SIZE/FAMILY OR IT WILL BE ONLY ONE-LINED DUE TO HEIGHT
    DETERMINATION DIFFICULTIES (using dummy nodes for it, but CSS doesn't really apply to them unless using setStyle)
    (ノಠ益ಠ)ノ彡┻━┻ ¯\_(ツ)_/¯
 */

public class RichText extends InlineCssTextArea {
    private static Label labelForSizing = new Label();
    private static Group groupForSizing = new Group();

    //One can get width/height of Node only when it is attached to the scene,
    //so there is a dummy scene
    private static Scene sceneForSizing = new Scene(groupForSizing, 500, 500);

    private static final double DEFAULT_MAX_WIDTH = 300;
    private double cachedMaxWidth;

    static{
        labelForSizing.setWrapText(true);
    }

    public RichText(String text, double maxWidth) {
        super(text);

        cachedMaxWidth = maxWidth;
        setDisable(true);
        setWrapText(true);
        size(maxWidth, null);

        maxWidthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                cachedMaxWidth = t1.doubleValue();
                size(cachedMaxWidth, getStyle());
            }
        });

        styleProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                size(cachedMaxWidth, t1);
            }
        });
    }

    public void size(double maxWidth, String style){
        labelForSizing.setMaxWidth(maxWidth);
        labelForSizing.setText(getText());
        labelForSizing.setStyle(style);

        groupForSizing.getChildren().add(labelForSizing);
        labelForSizing.applyCss();
        groupForSizing.layout();

        setPrefHeight(labelForSizing.getHeight());
        groupForSizing.getChildren().remove(labelForSizing);
    }

    public void size(){
        size(getMaxWidth(), getStyle());
    }

    public void size(String style){
        size(getMaxWidth(), style);
    }

    public void size(double maxWidth){
        size(maxWidth, getStyle());
    }
}