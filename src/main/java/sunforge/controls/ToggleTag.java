package sunforge.controls;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Button;

public class ToggleTag extends Button {
    private static String stylesheet;
    private BooleanProperty isActiveProperty = new SimpleBooleanProperty(true);

    public ToggleTag(String text) {
        super(text);
        configure();
    }

    private void configure(){
        if(stylesheet == null){
            stylesheet = ToggleTag.class.getResource("ToggleTag.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);
        getStyleClass().addAll("toggle-tag", "active");

        isActiveProperty.addListener((observableValue, wasActive, nowActive) -> {
            if(nowActive){
                getStyleClass().add("active");
            }else{
                getStyleClass().remove("active");
            }
        });

        setOnAction(actionEvent -> {
            isActiveProperty.set(!isActiveProperty.get());
        });
    }

    public boolean isActive() {
        return isActiveProperty.get();
    }

    public BooleanProperty isActiveProperty() {
        return isActiveProperty;
    }

    public void setIsActiveProperty(boolean isActiveProperty) {
        this.isActiveProperty.set(isActiveProperty);
    }
}