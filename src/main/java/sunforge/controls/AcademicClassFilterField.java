package sunforge.controls;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import sunforge.components.sidebar.SidebarAcademicViewPool;

public class AcademicClassFilterField extends VBox {
    private static String stylesheet = null;
    private Button advancedSearchButton;
    private TextField innerTextField;
    private SidebarAcademicViewPool associatedList = null;

    public AcademicClassFilterField() {
        this(null);
    }

    public AcademicClassFilterField(SidebarAcademicViewPool initialList) {
        if (stylesheet == null) {
            stylesheet = getClass().getResource("AcademicClassFilterField.css").toExternalForm();
        }

        //TODO MAKE THIS MODaL BUTTON ANOTHER COMPONENT
        advancedSearchButton = new Button("Гнучкий пошук");
        advancedSearchButton.setVisible(false);

        advancedSearchButton.getStyleClass().add("academic-class-filter-modal-button");

        innerTextField = new TextField();
        innerTextField.getStyleClass().add("academic-class-filter-text-field");

        innerTextField.focusedProperty().addListener((observableValue, wasFocused, isFocused) -> {
            advancedSearchButton.setVisible(isFocused);
        });

        widthProperty().addListener(e -> {
            advancedSearchButton.setMaxWidth(getWidth());
        });
        associatedList = initialList;

        innerTextField.textProperty().addListener((observableValue, oldText, newText) -> {
            if (associatedList != null) {
                associatedList.filterRender(academicClass -> academicClass.getSubjectName().toLowerCase().startsWith(newText.toLowerCase()));
            }
        });

        getChildren().addAll(advancedSearchButton, innerTextField);

    }

    public void bindListView(SidebarAcademicViewPool associatedList) {
        this.associatedList = associatedList;
    }

}
