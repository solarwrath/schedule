package sunforge.controls;

import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

class ArtificerInnerComboBox<T> extends ComboBox<T> {
    ArtificerInnerComboBox() {
        super();
        getStyleClass().add("artificer-combo-box");
        setCellFactory(t ->
                new ListCell<>() {
                    protected void updateItem(T item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            setGraphic(buildLayout(item));
                        } else {
                            setGraphic(null);
                        }
                    }
                }
        );

        //Enable autocompletion
        new AutoCompleteComboBoxListener<>(this);
    }

    private Node buildLayout(T item) {
        HBox layout = new HBox();
        Label label = new Label(getConverter().toString(item));
        label.getStyleClass().add("artificer-combo-box-label");

        layout.getChildren().add(label);

        //TODO TAGS HERE
        layout.getChildren().add(new Rectangle(16, 16, Color.RED));
        return layout;
    }
}