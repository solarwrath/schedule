package sunforge.controls;

import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.util.StringConverter;
import org.apache.poi.ss.formula.functions.T;
import sunforge.model.NoSuchRoomException;
import sunforge.model.Room;

public class RoomComboBox extends ArtificerComboBox<Room> {
    public RoomComboBox() {
        setItems(FXCollections.observableArrayList(Room.getPool()));

        //TODO This looks weird. Lookup solutions more???
        setConverter(new StringConverter<>() {
            @Override
            public String toString(Room room) {
                if (room != null) {
                    return room.getName();
                } else {
                    return null;
                }
            }

            @Override
            public Room fromString(String roomName) {
                try {
                    return Room.getRoomByName(roomName);
                } catch (IllegalArgumentException|NoSuchRoomException ex) {
                    return null;
                }
            }
        });

        new AutoCompleteComboBoxListener<>(getInnerComboBox());
    }
}