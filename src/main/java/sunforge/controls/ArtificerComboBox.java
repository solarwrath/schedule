package sunforge.controls;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.Effect;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.StringConverter;
import sunforge.App;
import sunforge.modals.FindRoomModal;
import sunforge.modals.ModalContent;
import sunforge.modals.ModalHolder;

public class ArtificerComboBox<T> extends VBox {
    private static String stylesheet = null;
    private ArtificerInnerComboBox<T> innerComboBox;
    private Button advancedSearchButton;

    public ArtificerComboBox() {
        if (stylesheet == null) {
            stylesheet = getClass().getResource("ArtificerComboBox.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);

        advancedSearchButton = new Button("Гнучкий пошук");
        advancedSearchButton.setVisible(false);
        advancedSearchButton.getStyleClass().add("artificer-combo-box-button");

        advancedSearchButton.setOnAction(actionEvent -> {
            Pane rootPane = ((Pane) getScene().getRoot());

            new ModalHolder(new FindRoomModal(this), rootPane);
        });

        innerComboBox = new ArtificerInnerComboBox<>();
        innerComboBox.focusedProperty().addListener((observableValue, wasFocused, isFocused) -> {
            advancedSearchButton.setVisible(isFocused);
        });

        innerComboBox.widthProperty().addListener(e -> {
            advancedSearchButton.setMaxWidth(innerComboBox.getWidth());
        });

        getChildren().addAll(advancedSearchButton, innerComboBox);
    }

    public ArtificerComboBox(final ObservableList<T> items) {
        this();
        setItems(items);
    }

    //TODO When advanced search button is moved to another component make this computable
    public static double getAdvancedSearchButtonHeight() {
        return 25;
    }

    public ArtificerInnerComboBox<T> getInnerComboBox() {
        return innerComboBox;
    }

    public Button getAdvancedSearchButton() {
        return advancedSearchButton;
    }

    public ObjectProperty<ListCell<T>> buttonCellProperty() {
        return innerComboBox.buttonCellProperty();
    }

    public ObjectProperty<Callback<ListView<T>, ListCell<T>>> cellFactoryProperty() {
        return innerComboBox.cellFactoryProperty();
    }

    public ObjectProperty<StringConverter<T>> converterProperty() {
        return innerComboBox.converterProperty();
    }

    public ReadOnlyObjectProperty<TextField> editorProperty() {
        return innerComboBox.editorProperty();
    }

    public ListCell<T> getButtonCell() {
        return innerComboBox.getButtonCell();
    }

    public void setButtonCell(ListCell<T> value) {
        innerComboBox.setButtonCell(value);
    }

    public Callback<ListView<T>, ListCell<T>> getCellFactory() {
        return innerComboBox.getCellFactory();
    }

    public void setCellFactory(Callback<ListView<T>, ListCell<T>> value) {
        innerComboBox.setCellFactory(value);
    }

    public StringConverter<T> getConverter() {
        return innerComboBox.getConverter();
    }

    public void setConverter(StringConverter<T> value) {
        innerComboBox.setConverter(value);
    }

    public TextField getEditor() {
        return innerComboBox.getEditor();
    }

    public ObservableList<T> getItems() {
        return innerComboBox.getItems();
    }

    public void setItems(ObservableList<T> value) {
        innerComboBox.setItems(value);
    }

    public Node getPlaceholder() {
        return innerComboBox.getPlaceholder();
    }

    public void setPlaceholder(Node value) {
        innerComboBox.setPlaceholder(value);
    }

    public SingleSelectionModel<T> getSelectionModel() {
        return innerComboBox.getSelectionModel();
    }

    public void setSelectionModel(SingleSelectionModel<T> value) {
        innerComboBox.setSelectionModel(value);
    }

    public int getVisibleRowCount() {
        return innerComboBox.getVisibleRowCount();
    }

    public void setVisibleRowCount(int value) {
        innerComboBox.setVisibleRowCount(value);
    }

    public ObjectProperty<ObservableList<T>> itemsProperty() {
        return innerComboBox.itemsProperty();
    }

    public ObjectProperty<Node> placeholderProperty() {
        return innerComboBox.placeholderProperty();
    }

    public Object queryAccessibleAttribute(AccessibleAttribute attribute, Object... parameters) {
        return innerComboBox.queryAccessibleAttribute(attribute, parameters);
    }

    public ObjectProperty<SingleSelectionModel<T>> selectionModelProperty() {
        return innerComboBox.selectionModelProperty();
    }

    public IntegerProperty visibleRowCountProperty() {
        return innerComboBox.visibleRowCountProperty();
    }

    public void arm() {
        innerComboBox.arm();
    }

    public BooleanProperty armedProperty() {
        return innerComboBox.armedProperty();
    }

    public void disarm() {
        innerComboBox.disarm();
    }

    public BooleanProperty editableProperty() {
        return innerComboBox.editableProperty();
    }

    public void executeAccessibleAction(AccessibleAction action, Object... parameters) {
        innerComboBox.executeAccessibleAction(action, parameters);
    }

    public String getPromptText() {
        return innerComboBox.getPromptText();
    }

    public void setPromptText(String value) {
        innerComboBox.setPromptText(value);
    }

    public T getValue() {
        return innerComboBox.getValue();
    }

    public void setValue(T value) {
        innerComboBox.setValue(value);
    }

    public void hide() {
        innerComboBox.hide();
    }

    public boolean isArmed() {
        return innerComboBox.isArmed();
    }

    public boolean isEditable() {
        return innerComboBox.isEditable();
    }

    public void setEditable(boolean value) {
        innerComboBox.setEditable(value);
    }

    public boolean isShowing() {
        return innerComboBox.isShowing();
    }

    public ObjectProperty<EventHandler<ActionEvent>> onActionProperty() {
        return innerComboBox.onActionProperty();
    }

    public ObjectProperty<EventHandler<Event>> onHiddenProperty() {
        return innerComboBox.onHiddenProperty();
    }

    public ObjectProperty<EventHandler<Event>> onHidingProperty() {
        return innerComboBox.onHidingProperty();
    }

    public ObjectProperty<EventHandler<Event>> onShowingProperty() {
        return innerComboBox.onShowingProperty();
    }

    public ObjectProperty<EventHandler<Event>> onShownProperty() {
        return innerComboBox.onShownProperty();
    }

    public StringProperty promptTextProperty() {
        return innerComboBox.promptTextProperty();
    }

    public void setOnAction(EventHandler<ActionEvent> value) {
        innerComboBox.setOnAction(value);
    }

    public void setOnHidden(EventHandler<Event> value) {
        innerComboBox.setOnHidden(value);
    }

    public void setOnHiding(EventHandler<Event> value) {
        innerComboBox.setOnHiding(value);
    }

    public void setOnShowing(EventHandler<Event> value) {
        innerComboBox.setOnShowing(value);
    }

    public void setOnShown(EventHandler<Event> value) {
        innerComboBox.setOnShown(value);
    }

    public void show() {
        innerComboBox.show();
    }

    public ReadOnlyBooleanProperty showingProperty() {
        return innerComboBox.showingProperty();
    }

    public ObjectProperty<T> valueProperty() {
        return innerComboBox.valueProperty();
    }

}
