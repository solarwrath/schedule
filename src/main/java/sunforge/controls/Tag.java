package sunforge.controls;

import javafx.scene.control.Button;

public class Tag extends Button {
    private static String stylesheet = null;

    public Tag() {
        this("Tag");
    }

    public Tag(String text) {
        super(text);

        if (stylesheet == null) {
            stylesheet = getClass().getResource("Tag.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);

        getStyleClass().add("tag");

        setFocusTraversable(false);
        setDisabled(true);
    }
}