package sunforge;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {
    public static void main( String[] args ){
        launch(args);
    }

    private static Stage singletoneStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        singletoneStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("ScheduleScene.fxml"));

        Scene scheduleScene = new Scene(root, 1920, 1080);
        scheduleScene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());

        primaryStage.setFullScreen(true);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(scheduleScene);

        primaryStage.show();
    }

    public static Stage getPrimaryStage(){
        return singletoneStage;
    }
}
