package sunforge.model;

public enum Day {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY;

    @Override
    public String toString() {
        switch (this) {
            case MONDAY:
                return "Понеділок";
            case TUESDAY:
                return "Вівторок";
            case WEDNESDAY:
                return "Середа";
            case THURSDAY:
                return "Четвер";
            case FRIDAY:
                return "П'ятниця";
            default:
                throw new IllegalArgumentException("There are no such days in the working week");
        }
    }
}