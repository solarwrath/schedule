package sunforge.model;

public enum AcademicClassType {
    LECTURE,
    PRACTICE;

    @Override
    public String toString() {
        switch (this) {
            case LECTURE:
                return "Лекція";
            case PRACTICE:
                return "Практика";
            default:
                throw new IllegalArgumentException("There are no such cases in AcademicClassType enum");
        }
    }
}