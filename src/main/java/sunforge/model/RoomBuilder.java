package sunforge.model;

public class RoomBuilder {
    private long ID;
    private String name;
    private boolean isActive;
    private int countComputers;
    private int countPlaces;
    private int block;
    private Room.RoomType type;

    public RoomBuilder(long ID, String name, boolean isActive, Room.RoomType type) {
        this.ID = ID;
        this.name = name;
        this.isActive = isActive;
        this.type = type;
    }

    public RoomBuilder setID(long id) {
        this.ID = id;
        return this;
    }

    public RoomBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public RoomBuilder setIsActive(boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public RoomBuilder setCountComputers(int countComputers) {
        this.countComputers = countComputers;
        return this;
    }

    public RoomBuilder setCountPlaces(int countPlaces) {
        this.countPlaces = countPlaces;
        return this;
    }

    public RoomBuilder setBlock(int block) {
        this.block = block;
        return this;
    }

    public Room build() {
        return new Room(ID, name, isActive, type, countComputers, countPlaces, block);
    }
}