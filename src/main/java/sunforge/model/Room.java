package sunforge.model;

import java.util.*;

public class Room {
    private final static Set<Room> pool = new HashSet<>();

    private final long ID;
    private final String name;
    private final int countComputers;
    private final int countPlaces;
    private final int block;
    //TODO mb change this for status
    private boolean isActive;
    private RoomType type;

    public Room(long ID, String name, boolean isActive, RoomType type, int countComputers, int countPlaces, int block) {
        this.ID = ID;
        this.name = name;
        this.isActive = isActive;
        this.type = type;
        this.countComputers = countComputers;
        this.countPlaces = countPlaces;
        this.block = block;

        pool.add(this);
    }

    public static Set<Room> getPool() {
        return pool;
    }

    public static Room getRoomById(long ID) throws NoSuchRoomException {
        for (Room currentRoom : pool) {
            if (currentRoom.ID == ID) {
                return currentRoom;
            }
        }

        throw new NoSuchRoomException(ID);
    }

    public static Room getRoomByName(String name) throws NoSuchRoomException {
        for (Room currentRoom : pool) {
            if (currentRoom.name.equals(name)) {
                return currentRoom;
            }
        }

        throw new NoSuchRoomException(name);
    }

    public long getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public int getCountComputers() {
        return countComputers;
    }

    public int getCountPlaces() {
        return countPlaces;
    }

    public int getBlock() {
        return block;
    }

    public boolean isActive() {
        return isActive;
    }

    public RoomType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Room{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", countComputers=" + countComputers +
                ", countPlaces=" + countPlaces +
                ", block=" + block +
                ", isActive=" + isActive +
                ", type=" + type +
                '}';
    }

    public enum RoomType {
        COMPUTER_CLASS,
        LECTURE_CLASS,
        GYM,
        LAB;

        private static Map<RoomType, String> descriptions =
                new HashMap<>(Map.of(
                        COMPUTER_CLASS, "Комп. клас",
                        LECTURE_CLASS, "Лекційна",
                        GYM, "Спортзал",
                        LAB, "Лабораторія"
                ));

        public static RoomType customValueOf(String description) {
            return descriptions
                    .entrySet()
                    .stream()
                    .filter(roomTypeStringEntry -> description.equals(roomTypeStringEntry.getValue()))
                    .findFirst()
                    .map(Map.Entry::getKey)
                    .orElseThrow(() -> new IllegalArgumentException("There are no such room types: " + description));
        }

        @Override
        public String toString() {
            return descriptions.get(this);
        }
    }
}