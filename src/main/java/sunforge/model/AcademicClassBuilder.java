package sunforge.model;

import java.util.List;

public class AcademicClassBuilder {
    private long ID;
    private String subjectName;
    private AcademicClassType type;
    private Week assignedWeek;
    private int indexInDay;
    private Room room;
    private Professor assignedProfessor;
    private List<AdditionalRequirement> additionalRequirements;
    private Subgroup subgroup;

    public AcademicClassBuilder(long ID, String subjectName, AcademicClassType type, Professor assignedProfessor) {
        this.ID = ID;
        this.subjectName = subjectName;
        this.type = type;
        this.assignedProfessor = assignedProfessor;
    }

    public AcademicClassBuilder setID(long ID) {
        this.ID = ID;
        return this;
    }

    public AcademicClassBuilder setWeek(Week week){
        this.assignedWeek = week;
        return this;
    }

    public AcademicClassBuilder setSubjectName(String subjectName) {
        this.subjectName = subjectName;
        return this;
    }

    public AcademicClassBuilder setType(AcademicClassType type) {
        this.type = type;
        return this;
    }

    public AcademicClassBuilder setIndexInDay(int indexInDay) {
        this.indexInDay = indexInDay;
        return this;
    }

    public AcademicClassBuilder setRoom(Room room) {
        this.room = room;
        return this;
    }

    public AcademicClassBuilder setAssignedProfessor(Professor professor) {
        this.assignedProfessor = professor;
        return this;
    }

    public AcademicClassBuilder setAdditionalRequirements(List<AdditionalRequirement> additionalRequirements) {
        this.additionalRequirements = additionalRequirements;
        return this;
    }

    public AcademicClassBuilder setSubgroup(Subgroup subgroup){
        this.subgroup = subgroup;
        return this;
    }

    public AcademicClass build() {
        return new AcademicClass(ID, subjectName, type, assignedWeek, indexInDay, room, assignedProfessor, additionalRequirements, subgroup);
    }
}