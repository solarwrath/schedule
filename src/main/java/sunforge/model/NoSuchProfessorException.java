package sunforge.model;

public class NoSuchProfessorException extends Exception {
    public NoSuchProfessorException() {
        super("Such professor doesn't exist!");
    }

    public NoSuchProfessorException(String name) {
        super("Professor " + name + " doesn't exist!");
    }

    public NoSuchProfessorException(long ID) {
        super("Professor with ID of " + ID + " doesn't exist!");
    }
}