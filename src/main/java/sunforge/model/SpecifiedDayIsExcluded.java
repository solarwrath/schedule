package sunforge.model;

public class SpecifiedDayIsExcluded extends Exception {
    public SpecifiedDayIsExcluded() {
        super("Specified day is excluded!");
    }

    public SpecifiedDayIsExcluded(Day day) {
        super(day.toString() + " is excluded!");
    }

    public SpecifiedDayIsExcluded(Day day, AcademicGroup academicGroup) {
        super(day.toString() + " is excluded in the " + academicGroup.getGroupName() + " group!");
    }
}