package sunforge.model;

public class NoSuchRoomException extends Exception {
    public NoSuchRoomException() {
        super("Such room doesn't exist!");
    }

    public NoSuchRoomException(String roomName) {
        super(roomName + " room doesn't exist!");
    }

    public NoSuchRoomException(long ID) {
        super("Room with ID of " + ID + " doesn't exist!");
    }
}