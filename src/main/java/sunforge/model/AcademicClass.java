package sunforge.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AcademicClass {
    private final static Set<AcademicClass> pool = new HashSet<>();

    private long ID;
    private String subjectName;
    private AcademicClassType type;
    private List<AdditionalRequirement> additionalRequirements;
    private Week assignedWeek;
    private int indexInDay;
    private Professor assignedProfessor;
    private ObjectProperty<Room> assignedRoom = new SimpleObjectProperty<>();
    private Subgroup subgroup;
    private boolean isEmpty = true;

    public AcademicClass(int indexInDay){
        this.indexInDay = indexInDay;
    }

    public AcademicClass(long ID, String subjectName, AcademicClassType type, Week assignedWeek, int indexInDay, Room room, Professor assignedProfessor, List<AdditionalRequirement> additionalRequirements, Subgroup subgroup) {
        this.ID = ID;
        this.subjectName = subjectName;
        this.type = type;
        this.assignedWeek = assignedWeek;
        this.indexInDay = indexInDay;
        assignedRoom.set(room);
        this.assignedProfessor = assignedProfessor;
        this.additionalRequirements = additionalRequirements;
        this.subgroup = subgroup;

        isEmpty = false;

        assignedRoomProperty().addListener((observableValue, oldRoom, newRoom) -> {
            //TODO Config here
            //TODO Insert this into dao
            String url = "jdbc:mysql://194.9.70.244:3306/Univer";
            String username = "anton";
            String password = "anton2019";

            try (Connection con = DriverManager.getConnection(url, username, password);
                 PreparedStatement ps = con.prepareStatement("UPDATE `classes` SET `room_id`=? WHERE `ID`=?")) {
                System.out.println("newRoom = " + newRoom);
                System.out.println("oldRoom = " + oldRoom);
                ps.setLong(1, newRoom.getID());
                ps.setLong(2, ID);
                ps.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });

        pool.add(this);
    }

    public static Set<AcademicClass> getPool() {
        return pool;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public long getID() {
        return ID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public AcademicClassType getType() {
        return type;
    }

    public int getIndexInDay() {
        return indexInDay;
    }

    public void setIndexInDay(int indexInDay) {
        this.indexInDay = indexInDay;
    }

    public Week getAssignedWeek() {
        return assignedWeek;
    }

    public void setAssignedWeek(Week assignedWeek) {
        this.assignedWeek = assignedWeek;
    }

    public Professor getProfessor() {
        return assignedProfessor;
    }

    public void setProfessor(Professor assignedProfessor) {
        this.assignedProfessor = assignedProfessor;
    }

    public Room getAssignedRoom() {
        return assignedRoom.get();
    }

    public ObjectProperty<Room> assignedRoomProperty() {
        return assignedRoom;
    }

    public void setAssignedRoom(Room assignedRoom) {
        this.assignedRoom.set(assignedRoom);
    }

    public List<AdditionalRequirement> getAdditionalRequirements() {
        return additionalRequirements;
    }

    public boolean hasAdditionalRequirement() {
        return additionalRequirements != null;
    }

    public Subgroup getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(Subgroup subgroup) {
        this.subgroup = subgroup;
    }

    @Override
    public String toString() {
        return "AcademicClass{" +
                "ID=" + ID +
                ", subjectName='" + subjectName + '\'' +
                ", type=" + type +
                ", additionalRequirements=" + additionalRequirements +
                ", assignedWeek=" + assignedWeek +
                ", indexInDay=" + indexInDay +
                ", assignedProfessor=" + assignedProfessor +
                ", assignedRoom=" + assignedRoom.get() +
                ", subgroup=" + subgroup +
                '}';
    }
}