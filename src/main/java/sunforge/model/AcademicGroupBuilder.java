package sunforge.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AcademicGroupBuilder {
    private long ID;
    private String groupName;
    private Day excludedDay;
    private List<AcademicClass> unassignedClasses = new ArrayList<>();
    private Map<Week, Map<Day, List<AcademicClass>>> assignedClasses = new HashMap<>();

    public AcademicGroupBuilder(long ID, String groupName, Day excludedDay) {
        this.ID = ID;
        this.groupName = groupName;
        this.excludedDay = excludedDay;
    }

    public AcademicGroupBuilder setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public AcademicGroupBuilder setExcludedDay(Day excludedDay) {
        this.excludedDay = excludedDay;
        return this;
    }

    public AcademicGroupBuilder setID(long ID) {
        this.ID = ID;
        return this;
    }

    public AcademicGroupBuilder setUnassignedClasses(List<AcademicClass> unassignedClasses) {
        this.unassignedClasses = unassignedClasses;
        return this;
    }

    public AcademicGroupBuilder setAssignedClasses(Map<Week, Map<Day, List<AcademicClass>>> assignedClasses) {
        this.assignedClasses = assignedClasses;
        return this;
    }

    public AcademicGroup build() {
        return new AcademicGroup(ID, groupName, excludedDay, unassignedClasses, assignedClasses);
    }
}