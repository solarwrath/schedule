package sunforge.model;

public class AdditionalRequirement {
    private final String description;

    public AdditionalRequirement(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}