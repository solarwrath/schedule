package sunforge.model;

import java.util.HashSet;
import java.util.Set;

public class Professor {
    private final static Set<Professor> pool = new HashSet<>();

    private final long ID;
    private final String name;

    public Professor(long ID, String name) {
        this.ID = ID;
        this.name = name;

        pool.add(this);
    }

    public static Set<Professor> getPool() {
        return pool;
    }

    public static Professor getProfessorByID(long ID) throws NoSuchProfessorException {
        for (Professor currentProfessor : pool) {
            if (currentProfessor.ID == ID) {
                return currentProfessor;
            }
        }

        throw new NoSuchProfessorException(ID);
    }

    public static Professor getProfessorByName(String name) throws NoSuchProfessorException {
        for (Professor currentProfessor : pool) {
            if (currentProfessor.name.equals(name)) {
                return currentProfessor;
            }
        }

        throw new NoSuchProfessorException(name);
    }

    public long getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Professor{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                '}';
    }
}