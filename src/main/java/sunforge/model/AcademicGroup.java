package sunforge.model;

import com.google.gson.Gson;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.*;

import java.sql.*;
import java.util.*;

public class AcademicGroup {
    private final static Set<AcademicGroup> pool = new HashSet<>();

    private final static ObjectProperty<AcademicGroup> currentGroupProperty = new SimpleObjectProperty<>();
    private final long ID;
    private final String groupName;
    private final Day excludedDay;
    //TODO Not sure about final here, consult
    private final ObservableList<AcademicClass> unassignedClasses;
    private final Map<Week, Map<Day, ObservableList<AcademicClass>>> assignedClasses;
    //private final HashMap<Day, ACList> assignedClasses;

    public AcademicGroup(long ID, String groupName, Day excludedDay, List<AcademicClass> unassignedClasses, Map<Week, Map<Day, List<AcademicClass>>> assignedClasses) {
        this.ID = ID;
        this.groupName = groupName;
        this.excludedDay = excludedDay;
        this.unassignedClasses = FXCollections.observableArrayList(unassignedClasses);

        this.assignedClasses = new HashMap<>();
        for (Week week : Week.values()) {
            Map<Day, ObservableList<AcademicClass>> tempDayMap = new HashMap<>();

            for (Day day : Day.values()) {
                ObservableList<AcademicClass> list = FXCollections.observableArrayList();

                for (int i = 0; i < 6; i++) {
                    list.add(new AcademicClass(i));
                }

                tempDayMap.put(day, list);
            }

            this.assignedClasses.put(week, tempDayMap);
        }

        if(assignedClasses != null){
            assignedClasses.forEach((week, dayListMap) -> {
                Map<Day, ObservableList<AcademicClass>> temp = new HashMap<>();

                dayListMap.forEach((day, academicClasses) -> {
                    temp.put(day, FXCollections.observableArrayList(academicClasses));
                });


                this.assignedClasses.put(week, temp);
            });
        }

        System.out.println("assignedClasses = " +new Gson().toJson(assignedClasses));

        /*
        this.assignedClasses = new HashMap<>();

        for (Day day : Day.values()) {
            if(day != excludedDay){
                List<AcademicClass> temp = new ArrayList<>();
                for (int i = 0; i < 6; i++) {
                    temp.add(new AcademicClass(i));
                }
                this.assignedClasses.put(day, new ACList(FXCollections.observableList(temp)));
            }
        }
        if(assignedClasses != null) {
            assignedClasses.forEach((day, classes) -> {
                for (int idx = 0; idx < classes.size(); idx++) {
                    this.assignedClasses.get(day).set(idx, classes.get(idx));
                }
            });
        }*/

        pool.add(this);
    }

    public static ObjectProperty<AcademicGroup> getCurrentGroupProperty() {
        return currentGroupProperty;
    }

    public static AcademicGroup getCurrentGroup() {
        return currentGroupProperty.getValue();
    }

    public static void setCurrentGroup(AcademicGroup academicGroup) {
        currentGroupProperty.setValue(academicGroup);
    }

    public static Set<AcademicGroup> getPool() {
        return pool;
    }

    public static AcademicGroup getGroupByID(long ID) throws NoSuchGroupException {
        for (AcademicGroup currentGroup : pool) {
            if (currentGroup.ID == ID) {
                return currentGroup;
            }
        }

        //TODO Not sure whether return null or throw Exception, consult

        throw new NoSuchGroupException(ID);
    }

    public static AcademicGroup getGroupByName(String groupName) throws NoSuchGroupException {
        for (AcademicGroup currentGroup : pool) {
            if (currentGroup.groupName.equals(groupName)) {
                return currentGroup;
            }
        }
        //TODO Not sure whether return null or throw Exception, consult

        throw new NoSuchGroupException(groupName);
    }

    public void setupListeners() {
        unassignedClasses.addListener(new ListChangeListener<AcademicClass>() {
            @Override
            public void onChanged(Change<? extends AcademicClass> change) {
                while (change.next()) {
                    /*
                    if (change.wasRemoved()) {
                        for (AcademicClass academicClass : change.getRemoved()) {
                            System.out.println("Removed from pool " + academicClass.getSubjectName());
                        }
                    }*/

                    if (change.wasAdded()) {
                        for (AcademicClass academicClass : change.getAddedSubList()) {
                            String url = "jdbc:mysql://194.9.70.244:3306/Univer";
                            String username = "anton";
                            String password = "anton2019";

                            try (Connection con = DriverManager.getConnection(url, username, password);
                                 PreparedStatement ps = con.prepareStatement("UPDATE `classes` SET `place`='POOL' WHERE `ID`=?")) {
                                ps.setLong(1, academicClass.getID());
                                ps.executeUpdate();
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }
            }
        });

        assignedClasses.forEach((week, dayObservableListMap) -> {
            dayObservableListMap.forEach((day, list) -> {
                list.addListener(new ListChangeListener<AcademicClass>() {
                    @Override
                    public void onChanged(Change<? extends AcademicClass> change) {
                        while (change.next()) {
                        /*if (change.wasRemoved()) {
                            for (AcademicClass academicClass : change.getRemoved()) {
                                if (academicClass != null) {
                                    System.out.println(day.toString() + " removed " + academicClass.getSubjectName());
                                }
                            }
                        }*/

                            if (change.wasAdded()) {
                                for (AcademicClass academicClass : change.getAddedSubList()) {
                                    if (academicClass != null) {
                                        String url = "jdbc:mysql://194.9.70.244:3306/Univer";
                                        String username = "anton";
                                        String password = "anton2019";

                                        String dayString = day.name();

                                        try (Connection con = DriverManager.getConnection(url, username, password);
                                             PreparedStatement ps = con.prepareStatement("UPDATE `classes` SET `place`=?,`index_in_day`=?, `week`=?, `subgroup`=? WHERE `ID`=?")) {
                                            ps.setString(1, dayString);
                                            ps.setInt(2, academicClass.getIndexInDay());
                                            ps.setString(3, academicClass.getAssignedWeek().name());
                                            ps.setString(4, academicClass.getSubgroup().name());
                                            ps.setLong(5, academicClass.getID());
                                            ps.executeUpdate();
                                        } catch (SQLException ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            });
        });
    }

    public long getID() {
        return ID;
    }

    public String getGroupName() {
        return groupName;
    }

    public Day getExcludedDay() {
        return excludedDay;
    }

    public List<AcademicClass> getUnassignedClasses() {
        return unassignedClasses;
    }

    public Map<Week, Map<Day, ObservableList<AcademicClass>>> getAssignedClasses() {
        return assignedClasses;
    }

    @Override
    public String toString() {
        return "AcademicGroup{" +
                "ID=" + ID +
                ", groupName='" + groupName + '\'' +
                ", excludedDay=" + excludedDay +
                ", unassignedClasses=" + unassignedClasses +
                ", assignedClasses=" + assignedClasses +
                '}';
    }
}