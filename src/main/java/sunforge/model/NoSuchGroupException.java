package sunforge.model;

public class NoSuchGroupException extends Exception {
    public NoSuchGroupException() {
        super("Such group doesn't exist!");
    }

    public NoSuchGroupException(String groupName) {
        super(groupName + " group doesn't exist!");
    }

    public NoSuchGroupException(long ID) {
        super("Group with ID of " + ID + " doesn't exist!");
    }
}