package sunforge;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.*;

import com.google.gson.Gson;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import sunforge.model.Day;
import sunforge.components.day.DayView;
import sunforge.components.sidebar.SidebarAcademicViewPool;
import sunforge.controls.AcademicClassFilterField;
import sunforge.controls.ArtificerComboBox;
import sunforge.dao.ImportService;
import sunforge.export.ExportService;
import sunforge.model.*;

public class ScheduleController {

    private static Week currentWeek = Week.FIRST;
    FileOutputStream out = null;
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private VBox sidebar;
    @FXML
    private ArtificerComboBox<AcademicGroup> groupComboBox;
    @FXML
    private AcademicClassFilterField academicFilterField;
    private SidebarAcademicViewPool sidebarAcademicViewPool;
    @FXML
    private TabPane weeksTabPane;
    @FXML
    private Tab weekAPane;
    @FXML
    private Tab weekBPane;
    private HBox weekA;
    private HBox weekB;

    public static Week getCurrentWeek() {
        return currentWeek;
    }

    @FXML
    void initialize() {
        ImportService.initializeData();

        weekA = new HBox();
        weekA.setAlignment(Pos.CENTER);
        weekA.setSpacing(10);
        weekA.setPadding(new Insets(10));

        weekB = new HBox();
        weekB.setAlignment(Pos.CENTER);
        weekB.setSpacing(10);
        weekB.setPadding(new Insets(10));

        weekAPane.setContent(weekA);
        weekBPane.setContent(weekB);
        weeksTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        //TODO Rewrite this to encapsulate logic. Make week field for extended hbox
        weeksTabPane.getSelectionModel().selectedItemProperty().addListener((observableValue, tab, t1) -> {
            currentWeek = currentWeek == Week.FIRST ? Week.SECOND : Week.FIRST;
        });

        //Make sidebar's maxHeight scale to the height of the scene;
        sidebar.sceneProperty().addListener((observableValue, oldScene, newScene) -> {
            newScene.heightProperty().addListener((observableValue2, oldHeight, newHeight) -> {
                sidebar.setPrefHeight(newHeight.doubleValue());
            });
        });

        sidebar.setPadding(new Insets(30, 30, 30, 30));

        groupComboBox.setConverter(new StringConverter<AcademicGroup>() {
            @Override
            public String toString(AcademicGroup academicGroup) {
                if (academicGroup != null) {
                    return academicGroup.getGroupName();
                }

                return null;
            }

            @Override
            public AcademicGroup fromString(String groupName) {
                try {
                    return AcademicGroup.getGroupByName(groupName);
                } catch (NoSuchGroupException ex) {
                    return null;
                }
            }
        });

        groupComboBox.getItems().addAll(AcademicGroup.getPool());
        groupComboBox.valueProperty().addListener((observableValue, oldAcademicGroup, newAcademicGroup) -> {
            if (newAcademicGroup == null) {
                groupComboBox.setValue(oldAcademicGroup);
            } else {
                AcademicGroup.setCurrentGroup(newAcademicGroup);
            }
        });

        AcademicGroup.getCurrentGroupProperty().addListener((observableValue, oldCurrentGroup, newCurrentGroup) -> {
            render();
        });

        sidebarAcademicViewPool = SidebarAcademicViewPool.getInstance();

        academicFilterField.bindListView(sidebarAcademicViewPool);
        sidebar.getChildren().add(sidebarAcademicViewPool);


        Button export = new Button("Export");
        export.setOnAction(actionEvent -> {
            FileChooser fileChooser = new FileChooser();

            //Set extension filter for text files
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xlsx)", "*.xlsx");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fileChooser.showSaveDialog(sidebar.getScene().getWindow());

            if (file != null) {
                ExportService.export(file);
            }
        });

        sidebar.getChildren().addAll(export);

        groupComboBox.getSelectionModel().select(0);

        Button button = new Button("Test");
        button.setOnAction(actionEvent -> {
            System.out.println("currentWeek = " + currentWeek);

            AcademicGroup.getCurrentGroup().getAssignedClasses().get(currentWeek).forEach((day, list) -> {
                System.out.println("day = " + day);
                System.out.println(list);
                System.out.println("------------------------------------");
            });
        });

        sidebar.getChildren().add(button);
    }

    private void render() {
        render(weekA);
        render(weekB);
    }

    private void render(HBox container) {
        container.getChildren().clear();

        List<Day> list = new ArrayList<>(List.of(Day.MONDAY, Day.TUESDAY, Day.WEDNESDAY, Day.THURSDAY, Day.FRIDAY));
        list.remove(AcademicGroup.getCurrentGroup().getExcludedDay());

        Week week = container.equals(weekA) ? Week.FIRST : Week.SECOND;

        list.forEach(day -> {

            System.out.println("academicGroup = " + new Gson().toJson(AcademicGroup.getCurrentGroup().getAssignedClasses()));
            container.getChildren().add(
                    new DayView(week, day,
                            AcademicGroup
                                    .getCurrentGroup()
                                    .getAssignedClasses()
                                    .get(week)
                                    .get(day)
                    )
            );
        });

        sidebarAcademicViewPool.getBoundClasses().setAll(AcademicGroup.getCurrentGroup().getUnassignedClasses());
    }
}