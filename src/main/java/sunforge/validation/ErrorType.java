package sunforge.validation;

public enum ErrorType {
    WARNING,
    FATAL;
}