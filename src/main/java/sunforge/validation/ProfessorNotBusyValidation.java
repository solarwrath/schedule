package sunforge.validation;

import sunforge.model.Day;
import sunforge.components.day.DayAcademicViewWrapper;
import sunforge.model.AcademicClass;
import sunforge.model.AcademicGroup;
import sunforge.model.Professor;

import java.util.List;

public class ProfessorNotBusyValidation extends AbstractValidation {
    @Override
    public ErrorType getErrorType() {
        return ErrorType.FATAL;
    }

    @Override
    public boolean validate(DayAcademicViewWrapper academicViewWrapperToTest) {
        AcademicClass academicClass = academicViewWrapperToTest.getFirstAcademicClass();
        Professor professorToSeek = academicClass.getProfessor();
        Day day = academicViewWrapperToTest.getDay();
        int academicViewIndexInParent = academicViewWrapperToTest.getParent().getChildrenUnmodifiable().indexOf(academicViewWrapperToTest);

        for (AcademicGroup academicGroup : AcademicGroup.getPool()) {

            if (AcademicGroup.getCurrentGroup() != academicGroup) {

                if (academicGroup.getExcludedDay() != day) {

                    List<AcademicClass> assignedAcademicClasses = academicGroup.getAssignedClasses().get(academicClass.getAssignedWeek()).  get(day);
                    if (assignedAcademicClasses.size() > academicViewIndexInParent) {

                        AcademicClass correspondingAcademicClass = assignedAcademicClasses.get(academicViewIndexInParent);

                        if (correspondingAcademicClass != null) {
                            if (correspondingAcademicClass.getProfessor() == professorToSeek) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}