package sunforge.validation;

import sunforge.components.day.DayAcademicViewWrapper;

abstract class AbstractValidation {
    private static ErrorType errorType;

    AbstractValidation() {
        errorType = getErrorType();
    }

    public abstract ErrorType getErrorType();

    public abstract boolean validate(DayAcademicViewWrapper academicViewWrapper);
}