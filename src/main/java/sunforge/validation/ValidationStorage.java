package sunforge.validation;

import sunforge.components.day.DayAcademicViewWrapper;

import java.util.HashSet;
import java.util.Set;

public class ValidationStorage {
    private Set<AbstractValidation> validationsToRun;

    private ValidationStorage() {
        validationsToRun = new HashSet<>();
        validationsToRun.add(new ProfessorNotBusyValidation());
    }

    public static ValidationStorage getInstance() {
        return ValidationStorageSingletonHolder.VALIDATION_INSTANCE;
    }

    public void runAllValidations(DayAcademicViewWrapper academicViewWrapperToValidate) {
        for (AbstractValidation currentValidation : validationsToRun) {
            if (!currentValidation.validate(academicViewWrapperToValidate)) {
                switch (currentValidation.getErrorType()) {
                    case FATAL:
                        academicViewWrapperToValidate.getStyleClass().add("error-fatal");
                        break;
                    case WARNING:
                        academicViewWrapperToValidate.getStyleClass().add("error-warning");
                        break;
                    default:
                        throw new IllegalArgumentException("There are no such error types");
                }
                break;
            }
        }
    }

    private static class ValidationStorageSingletonHolder {
        private static final ValidationStorage VALIDATION_INSTANCE = new ValidationStorage();
    }
}