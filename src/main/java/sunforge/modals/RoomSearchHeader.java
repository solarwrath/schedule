package sunforge.modals;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextAlignment;

public class RoomSearchHeader extends StackPane {
    private static String stylesheet;

    public RoomSearchHeader(String label, double width) {
        if (stylesheet == null) {
            stylesheet = RoomSearchHeader.class.getResource("RoomSearchHeader.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);

        getStyleClass().add("room-search-header");

        setAlignment(Pos.BASELINE_CENTER);
        setPrefWidth(width);

        Label labelNode = new Label(label);
        labelNode.setWrapText(true);
        labelNode.setTextAlignment(TextAlignment.CENTER);

        StackPane.setAlignment(labelNode, Pos.BASELINE_CENTER);

        getChildren().add(labelNode);
    }
}
