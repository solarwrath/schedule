package sunforge.modals;

import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class ModalHolder extends GridPane {
    private Pane rootPane;

    public ModalHolder(ModalContent modalContent, Pane rootPane) {
        Parent content = modalContent.render();

        sceneProperty().addListener((observableValue, oldScene, newScene) -> {
            if(newScene != null) {
                setPrefHeight(newScene.getHeight());
                setPrefWidth(newScene.getWidth());
            }
        });

        this.rootPane = rootPane;

        setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getTarget() == this) {
                close();
            }
        });

        setStyle("-fx-background-color: rgba(0, 0, 0, 0.5);");
        setAlignment(Pos.CENTER);

        getChildren().add(content);

        rootPane.getChildren().add(this);
    }

    public void close(){
        rootPane.getChildren().remove(this);
    }
}