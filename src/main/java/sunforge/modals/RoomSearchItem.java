package sunforge.modals;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import sunforge.model.Room;

public class RoomSearchItem extends HBox {
    private static String stylesheet = null;
    private static RoomSearchItem selectedSearchItem = null;
    private Room associatedRoom;
    private Label nameLabel = new Label();
    private Label typeLabel = new Label();
    private StackPane typePane = new StackPane(typeLabel);
    private Label countPlacesLabel = new Label();
    private StackPane countPlacesPane = new StackPane(countPlacesLabel);
    private Label countPCsLabel = new Label();
    private StackPane countPCsPane = new StackPane(countPCsLabel);

    RoomSearchItem() {
        if (stylesheet == null) {
            stylesheet = RoomSearchItem.class.getResource("RoomSearchItem.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);
        getStyleClass().add("room-search-item");

        setFocusTraversable(true);

        setOnMouseClicked(mouseEvent -> {
            Platform.runLater(() -> requestFocus());
        });
        focusedProperty().addListener((observableValue, wasFocused, isFocused) -> {
            if (isFocused) {
                selectedSearchItem = this;
                getStyleClass().add("room-search-item-selected");
            } else {
                getStyleClass().remove("room-search-item-selected");
            }
        });

        nameLabel.setPrefWidth(100);
        nameLabel.setWrapText(true);
        nameLabel.setAlignment(Pos.CENTER);

        typeLabel.setWrapText(true);
        typeLabel.setOnMouseClicked(mouseEvent -> {
            requestFocus();
        });
        typePane.setPrefWidth(152);
        typePane.setAlignment(Pos.CENTER);

        countPlacesLabel.setWrapText(true);
        countPlacesPane.setPrefWidth(62);
        countPlacesPane.setAlignment(Pos.CENTER);

        countPCsLabel.setWrapText(true);
        countPCsPane.setPrefWidth(85);
        countPCsPane.setAlignment(Pos.CENTER);
    }

    RoomSearchItem(Room roomToDisplay) {
        this();

        associatedRoom = roomToDisplay;

        render();
    }

    public static RoomSearchItem getSelectedSearchItem() {
        return selectedSearchItem;
    }

    private void render() {
        getChildren().clear();

        nameLabel.setText(associatedRoom.getName());
        typeLabel.setText(associatedRoom.getType().toString());
        countPlacesLabel.setText(String.valueOf(associatedRoom.getCountPlaces()));
        countPCsLabel.setText(String.valueOf(associatedRoom.getCountComputers()));

        getChildren().addAll(nameLabel, typePane, countPlacesPane, countPCsPane);
    }

    public Room getRoom() {
        return associatedRoom;
    }

    public void setRoom(Room room) {
        associatedRoom = room;
    }
}