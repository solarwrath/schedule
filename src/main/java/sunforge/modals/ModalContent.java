package sunforge.modals;

import javafx.scene.Parent;

public abstract class ModalContent {
    public abstract Parent render();
}