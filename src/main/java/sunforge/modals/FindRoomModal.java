package sunforge.modals;

import com.jfoenix.controls.JFXSlider;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sunforge.controls.ArtificerComboBox;
import sunforge.controls.ToggleTag;
import sunforge.model.Room;

import java.util.List;

public class FindRoomModal extends ModalContent {
    private static final int INITIAL_MIN_COUNT_PLACES = 0;
    private static final int INITIAL_MAX_COUNT_PLACES = 150;
    private static final double PSEUDO_ACCORDION_MAX_WIDTH = 250;
    private static String stylesheet = null;
    private ArtificerComboBox boundComboBox;
    private VBox searchResultsLayout;
    private DataEntity dataEntity = new DataEntity();

    public FindRoomModal(ArtificerComboBox boundComboBox) {
        if (stylesheet == null) {
            stylesheet = FindRoomModal.class.getResource("Modal.css").toExternalForm();
        }

        this.boundComboBox = boundComboBox;

        dataEntity.activeFilteringTags.addListener((ListChangeListener<ToggleTag>) change -> updateSearchResult());

        dataEntity.minPlaces.addListener((observableValue, pldMinPlace, newMinPlace) -> {
            updateSearchResult();
        });
    }

    public Parent render() {
        VBox mainLayout = new VBox();
        mainLayout.getStylesheets().add(stylesheet);
        mainLayout.getStyleClass().addAll("modal-parent", "modal-find-room");

        HBox mainContent = new HBox(10);

        mainContent.getChildren().addAll(renderFiltersPart(), renderSearchPart());
        VBox.setMargin(mainContent, new Insets(10, 0, 0, 0));

        Label title = new Label("Пошук аудиторії");
        title.getStyleClass().add("modal-title");

        HBox titleHolder = new HBox(title);
        titleHolder.setPrefWidth(mainLayout.getPrefWidth());
        titleHolder.setAlignment(Pos.CENTER);
        VBox.setMargin(titleHolder, new Insets(10, 0, 0, 0));

        Button confirmButton = new Button("Зберігти");
        confirmButton.setOnAction(actionEvent -> {
            //TODO Decide whether we should keep the value that is just entered and doesn't belong to any class
            if (RoomSearchItem.getSelectedSearchItem() != null) {
                boundComboBox.setValue(RoomSearchItem.getSelectedSearchItem().getRoom());
            }
            ((ModalHolder) mainLayout.getParent()).close();
        });

        Button cancelButton = new Button("Скасувати");
        cancelButton.setOnAction(actionEvent -> {
            //TODO avoid leak i guess
            ((ModalHolder) mainLayout.getParent()).close();
        });

        cancelButton.focusedProperty().addListener((observableValue, wasFocused, isFocused) -> {
            if (!isFocused) {
                mainLayout.requestFocus();
            }
        });

        HBox buttonHolder = new HBox(10);
        buttonHolder.setPrefWidth(mainLayout.getPrefWidth());
        buttonHolder.setAlignment(Pos.CENTER);
        VBox.setMargin(buttonHolder, new Insets(10, 0, 0, 0));

        buttonHolder.getChildren().addAll(confirmButton, cancelButton);

        mainLayout.getChildren().addAll(titleHolder, mainContent, buttonHolder);

        //Get focus on search field
        //Can't just requestFocus() it as at this moment nodes are not ready
        //On SO people said that may need to repeat it multiple times in case of strange architecture
        Platform.runLater(() -> {
            if (!dataEntity.searchField.isFocused()) {
                dataEntity.searchField.requestFocus();
            }
        });

        return mainLayout;
    }

    private VBox renderSearchPart() {
        VBox searchPart = new VBox();

        dataEntity.searchField = new TextField();
        HBox searchResultsHeadersPane = new HBox();
        VBox.setMargin(searchResultsHeadersPane, new Insets(10, 0, 0, 0));

        searchResultsHeadersPane.getChildren().addAll(
                new RoomSearchHeader("Назва аудиторії", 102),
                new RoomSearchHeader("Тип", 152),
                new RoomSearchHeader("Кількість місць", 62),
                new RoomSearchHeader("Кількість комп'ютерів", 87));

        //Add right border to the last header
        searchResultsHeadersPane
                .getChildren()
                .get(searchResultsHeadersPane.getChildrenUnmodifiable().size() - 1)
                .setStyle("-fx-border-width: 1px;");

        ScrollPane searchResultsWrapper = new ScrollPane();
        VBox.setMargin(searchResultsWrapper, new Insets(-2, 0, 0, 0));
        searchResultsWrapper.setMaxHeight(350);
        searchResultsWrapper.setFocusTraversable(false);
        //TODO hack cause of stupid ScrollPane behaviour, change it if move to using sources
        searchResultsWrapper.setStyle(
                "-fx-focus-color: transparent;" +
                        "-fx-faint-focus-color: transparent;"
        );

        searchResultsLayout = new VBox();

        dataEntity.searchField.textProperty().addListener((observableValue, oldSearchQuery, newSearchQuery) -> {
            updateSearchResult();
        });

        searchResultsWrapper.setContent(searchResultsLayout);

        updateSearchResult();

        searchPart.getChildren().addAll(dataEntity.searchField, searchResultsHeadersPane, searchResultsWrapper);

        return searchPart;
    }

    private VBox renderFiltersPart() {
        VBox filtersPart = new VBox(10);
        filtersPart.setMaxWidth(PSEUDO_ACCORDION_MAX_WIDTH);

        //Filter type

        TitledPane filterTypePane = new TitledPane();
        filterTypePane.setText("Тип");

        FlowPane typeContent = new FlowPane();
        typeContent.setVgap(5);
        typeContent.setHgap(5);
        typeContent.setAlignment(Pos.CENTER);

        ObservableList<Node> typeContentChildren = typeContent.getChildren();
        for (Room.RoomType roomType : Room.RoomType.values()) {
            ToggleTag toggleTag = new ToggleTag(roomType.toString());
            dataEntity.activeFilteringTags.add(toggleTag);

            toggleTag.isActiveProperty().addListener((observableValue, wasActive, isActive) -> {
                if (isActive) {
                    dataEntity.activeFilteringTags.add(toggleTag);
                } else {
                    dataEntity.activeFilteringTags.remove(toggleTag);
                }
            });

            typeContentChildren.add(toggleTag);
        }

        filterTypePane.setContent(typeContent);

        //Filter by min count places
        TitledPane filterMinCountPlaces = new TitledPane();
        filterMinCountPlaces.setText("Мінімальна кількість місць");

        VBox minCountPlacesContent = new VBox(5);
        HBox minCountPlacesFieldPane = new HBox(5);
        TextField countPlacesField = new TextField();

        minCountPlacesFieldPane.getChildren().addAll(new Label("Від:"), countPlacesField);

        JFXSlider minCountPlacesSlider = new JFXSlider();
        VBox.setMargin(minCountPlacesSlider, new Insets(5, 0, 0, 0));

        minCountPlacesSlider.setValue(0);
        minCountPlacesSlider.setMin(INITIAL_MIN_COUNT_PLACES);
        minCountPlacesSlider.setMax(INITIAL_MAX_COUNT_PLACES);

        minCountPlacesSlider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            countPlacesField.setText(String.valueOf(Math.round(newValue.doubleValue())));
        });

        countPlacesField.textProperty().addListener((observableValue, oldValueString, newValueStringed) -> {
            if (!newValueStringed.matches("\\d*")) {
                newValueStringed = newValueStringed.replaceAll("[^\\d]", "");
                countPlacesField.setText(newValueStringed);
            }

            if (!newValueStringed.isEmpty()) {
                minCountPlacesSlider.setValue(Double.parseDouble(newValueStringed));
                dataEntity.minPlaces.set(Integer.parseInt(newValueStringed));
            }
        });

        minCountPlacesContent.getChildren().addAll(minCountPlacesFieldPane, minCountPlacesSlider);
        filterMinCountPlaces.setContent(minCountPlacesContent);

        filtersPart.getChildren().addAll(filterTypePane, filterMinCountPlaces);

        return filtersPart;
    }

    private void updateSearchResult() {
        if (searchResultsLayout != null) {
            searchResultsLayout.getChildren().clear();

            ObservableList<ToggleTag> activeFilteringTags = dataEntity.activeFilteringTags;
            int minPlaces = dataEntity.minPlaces.get();
            String newSearchQuery = dataEntity.searchField.getText();

            for (Room currentRoom : Room.getPool()) {
                if (currentRoom.getName().trim().toLowerCase().startsWith(newSearchQuery.trim().toLowerCase())) {
                    if (currentRoom.getCountPlaces() >= minPlaces) {
                        if (typeAccepted(currentRoom.getType(), activeFilteringTags)) {
                            searchResultsLayout.getChildren().add(new RoomSearchItem(currentRoom));
                        }
                    }
                }
            }

            int resultsSize = searchResultsLayout.getChildrenUnmodifiable().size();
            if (resultsSize > 0) {
                searchResultsLayout.getChildrenUnmodifiable().get(searchResultsLayout.getChildrenUnmodifiable().size() - 1).getStyleClass().add("last");
            }
        }
    }

    private boolean typeAccepted(Room.RoomType roomType, List<ToggleTag> activeFilteringTags) {
        for (ToggleTag activeFilteringTag : activeFilteringTags) {
            if (activeFilteringTag.getText().equals(roomType.toString())) {
                return true;
            }
        }
        return false;
    }

    public ArtificerComboBox getBoundComboBox() {
        return boundComboBox;
    }

    public void setBoundComboBox(ArtificerComboBox boundComboBox) {
        this.boundComboBox = boundComboBox;
    }

    private class DataEntity {
        private ObservableList<ToggleTag> activeFilteringTags = FXCollections.observableArrayList();
        private IntegerProperty minPlaces = new SimpleIntegerProperty(0);
        private TextField searchField;
    }
}