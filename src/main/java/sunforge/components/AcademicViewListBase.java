package sunforge.components;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import sunforge.model.AcademicClass;

import java.util.List;

public abstract class AcademicViewListBase extends ScrollPane {
    private static String stylesheet = null;
    protected VBox underlyingVBox = new VBox(5);

    protected ObservableList<AcademicClass> boundClasses = FXCollections.observableArrayList();

    public AcademicViewListBase(List<AcademicClass> academicClasses) {
        if (stylesheet == null) {
            stylesheet = AcademicViewListBase.class.getResource("AcademicViewListBase.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);
        getStyleClass().add("academic-view-list");

        boundClasses.setAll(academicClasses);

        //TODO Create factory and invoke render from it
        setContent(underlyingVBox);
    }

    protected void initializeListener(){
        boundClasses.addListener(new ListChangeListener<AcademicClass>() {
            @Override
            public void onChanged(Change<? extends AcademicClass> change) {
                render();
            }
        });
        render();
    }

    protected abstract void render(List<AcademicClass> academicClasses);

    public ObservableList<AcademicClass> getBoundClasses() {
        return boundClasses;
    }

    private void render() {
        underlyingVBox.getChildren().clear();
        render(boundClasses);
    }
}