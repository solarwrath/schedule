package sunforge.components;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.skin.TabPaneSkin;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import sunforge.model.AcademicClass;
import sunforge.model.Subgroup;


public abstract class AcademicViewWrapperBase extends StackPane {
    protected ObjectProperty<AcademicClass> firstAcademicClass = new SimpleObjectProperty<>();
    protected ObjectProperty<AcademicClass> secondAcademicClass = new SimpleObjectProperty<>();
    private static String stylesheet;

    protected AcademicViewContentBase firstSubgroupContent;
    protected AcademicViewContentBase secondSubgroupContent;
    protected Mode mode;

    public AcademicViewWrapperBase(AcademicClass academicClass) {
        if(stylesheet == null){
            stylesheet = AcademicViewWrapperBase.class.getResource("AcademicViewWrapperBase.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);

        firstAcademicClass.set(academicClass);

        firstSubgroupContent = createAcademicViewContent(firstAcademicClass, Subgroup.BOTH);

        mode = Mode.GROUP;

        render();
    }

    public AcademicViewWrapperBase(AcademicClass firstSubgroupClass, AcademicClass secondSubgroupClass) {
        if(stylesheet == null){
            stylesheet = AcademicViewWrapperBase.class.getResource("AcademicViewWrapperBase.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);

        firstAcademicClass.set(firstSubgroupClass);
        secondAcademicClass.set(secondSubgroupClass);

        firstSubgroupContent = createAcademicViewContent(firstAcademicClass, Subgroup.FIRST);
        secondSubgroupContent = createAcademicViewContent(secondAcademicClass, Subgroup.SECOND);

        mode = Mode.SUBGROUP;

        render();
    }

    protected abstract AcademicViewContentBase createAcademicViewContent(ObjectProperty<AcademicClass> academicClassProperty, Subgroup subgroup);

    private void render() {
        getChildren().clear();

        if (mode == Mode.GROUP) {
            renderGroupMode();
        } else {
            renderSubgroupMode();
        }
    }

    protected void renderGroupMode() {
        getChildren().add(firstSubgroupContent);
    }

    protected void renderSubgroupMode() {
        TabPane tabPane = new TabPane();

        tabPane.getStylesheets().add(stylesheet);
        tabPane.setSide(Side.LEFT);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        tabPane.setPrefWidth(310);

        Tab firstSubgroupTab = new Tab("I");
        firstSubgroupTab.setContent(firstSubgroupContent);

        Tab secondSubgroupTab = new Tab("II");
        secondSubgroupTab.setContent(secondSubgroupContent);

        tabPane.getTabs().addAll(firstSubgroupTab, secondSubgroupTab);

        getChildren().add(tabPane);

        heightProperty().addListener(new ChangeListener<Number>() {
            private boolean checked = false;

            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                if (!checked) {
                    checked = true;
                    tabPane.setTabMaxWidth(t1.doubleValue() / 2);
                    tabPane.setTabMinWidth(t1.doubleValue() / 2);
                }
            }
        });
    }

    private enum Mode {
        GROUP,
        SUBGROUP
    }


    public AcademicClass getFirstAcademicClass() {
        return firstAcademicClass.get();
    }

    public ObjectProperty<AcademicClass> firstAcademicClassProperty() {
        return firstAcademicClass;
    }

    public AcademicClass getSecondAcademicClass() {
        return secondAcademicClass.get();
    }

    public ObjectProperty<AcademicClass> secondAcademicClassProperty() {
        return secondAcademicClass;
    }
}