package sunforge.components.sidebar;

import javafx.beans.property.ObjectProperty;
import javafx.scene.SnapshotParameters;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import sunforge.components.AcademicViewContentBase;
import sunforge.model.AcademicClass;
import sunforge.model.AcademicGroup;
import sunforge.model.Subgroup;
import sunforge.utils.LocalDragboard;

public class SidebarAcademicViewContent extends AcademicViewContentBase {
    public SidebarAcademicViewContent(ObjectProperty<AcademicClass> academicClassProperty, Subgroup subgroup) {
        super(academicClassProperty, subgroup);
    }

    @Override
    protected void makeDraggable() {
        setOnDragDetected(mouseEvent -> {
            Dragboard db = startDragAndDrop(TransferMode.ANY);

            SnapshotParameters snapshotParameters = new SnapshotParameters();
            snapshotParameters.setFill(Color.TRANSPARENT);
            db.setDragView(snapshot(snapshotParameters, null));

            ClipboardContent clipboardContent = new ClipboardContent();
            clipboardContent.putString("AcademicClassDnD");

            db.setContent(clipboardContent);

            LocalDragboard.getInstance().putValue(AcademicClass.class, academicClassProperty.getValue());
            mouseEvent.consume();
        });

        setOnDragDone(dragEvent -> {
            LocalDragboard localDragboard = LocalDragboard.getInstance();
            if (dragEvent.isAccepted() && !localDragboard.hasType(Boolean.class)) {
                //TODO Make sidebar rerender on classes in pool changes.
                if(!academicClassProperty.get().isEmpty()) {
                    SidebarAcademicViewPool.getInstance().getBoundClasses().remove(academicClassProperty.get());
                    AcademicGroup.getCurrentGroup().getUnassignedClasses().remove(academicClassProperty.get());
                }
                academicClassProperty.set(null);
            }

            if (dragEvent.isAccepted() && localDragboard.hasType(String.class)) {
                academicClassProperty.set(null);
            }

            localDragboard.clearAll();

            dragEvent.consume();
        });
    }

    @Override
    protected void makeDroppable() {

    }
}
