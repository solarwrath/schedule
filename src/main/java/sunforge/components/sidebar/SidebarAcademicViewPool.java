package sunforge.components.sidebar;

import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import sunforge.components.AcademicViewListBase;
import sunforge.model.AcademicClass;
import sunforge.model.AcademicGroup;
import sunforge.utils.LocalDragboard;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SidebarAcademicViewPool extends AcademicViewListBase {
    private SidebarAcademicViewPool(List<AcademicClass> data) {
        super(data);

        initializeListener();

        makeDroppable();
    }

    @Override
    protected void render(List<AcademicClass> academicClasses) {
        //TODO Subgroups

        if(academicClasses != null){
            academicClasses.forEach(academicClass -> {
                underlyingVBox.getChildren().add(new SidebarAcademicViewWrapper(academicClass));
            });
        }
    }

    public void filterRender(Predicate<? super AcademicClass> predicate) {
        //Render all items that pass the predicate, doesn't change the data itself
        //I am not happy with creating new AcademicViews, but don't see an option to rearrange
        //only visible AcademicViews. Potential TODO.
        List<AcademicClass> temp = new ArrayList<>(boundClasses);
        temp.removeIf(predicate.negate());
        render(temp);
    }

    public static SidebarAcademicViewPool getInstance() {
        return SidebarAcademicViewPoolHolder.HOLDER_INSTANCE;
    }

    private void makeDroppable() {
        setOnDragOver(dragEvent -> {
            Dragboard db = dragEvent.getDragboard();
            if (LocalDragboard.getInstance().hasType(AcademicClass.class) &&
                    !getBoundClasses().contains(LocalDragboard.getInstance().getValue(AcademicClass.class))
                    && db.hasString() && db.getString().equals("AcademicClassDnD")) {
                dragEvent.acceptTransferModes(TransferMode.ANY);
            }

            dragEvent.consume();
        });

        setOnDragDropped(dragEvent -> {
            Dragboard db = dragEvent.getDragboard();
            boolean success = false;

            LocalDragboard localDragboard = LocalDragboard.getInstance();

            if (db.hasString() && db.getString().equals("AcademicClassDnD") && localDragboard.hasType(AcademicClass.class)) {
                //Return current class to the pool
                AcademicClass classFromDragboard = localDragboard.getValue(AcademicClass.class);
                if (!getBoundClasses().contains(classFromDragboard)) {
                    localDragboard.putValue(String.class, "Wasn't already in sidebar");
                }

                classFromDragboard.setAssignedWeek(null);
                AcademicGroup.getCurrentGroup().getUnassignedClasses().add(classFromDragboard);
                //TODO Nuke this when rerender implemented to sidebar academic view pool on boundClasses change
                getBoundClasses().add(classFromDragboard);

                localDragboard.clear(AcademicClass.class);

                localDragboard.putValue(Boolean.class, true);
                success = true;
            }

            dragEvent.setDropCompleted(success);

            dragEvent.consume();
        });
    }

    private static class SidebarAcademicViewPoolHolder {
        public static SidebarAcademicViewPool HOLDER_INSTANCE = new SidebarAcademicViewPool(new ArrayList<>());
    }
}
