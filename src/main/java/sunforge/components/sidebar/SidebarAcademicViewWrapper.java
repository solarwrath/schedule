package sunforge.components.sidebar;

import javafx.beans.property.ObjectProperty;
import sunforge.components.AcademicViewContentBase;
import sunforge.components.AcademicViewWrapperBase;
import sunforge.model.AcademicClass;
import sunforge.model.Subgroup;

public class SidebarAcademicViewWrapper extends AcademicViewWrapperBase {

    public SidebarAcademicViewWrapper(AcademicClass academicClass) {
        super(academicClass);
    }

    public SidebarAcademicViewWrapper(AcademicClass firstSubgroupClass, AcademicClass secondSubgroupClass) {
        super(firstSubgroupClass, secondSubgroupClass);
    }

    @Override
    protected AcademicViewContentBase createAcademicViewContent(ObjectProperty<AcademicClass> academicClassProperty, Subgroup subgroup) {
        return new SidebarAcademicViewContent(academicClassProperty, subgroup);
    }
}
