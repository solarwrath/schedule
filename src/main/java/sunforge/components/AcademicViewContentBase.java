package sunforge.components;

import javafx.beans.property.ObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import sunforge.components.sidebar.SidebarAcademicViewPool;
import sunforge.controls.ArtificerComboBox;
import sunforge.controls.RichText;
import sunforge.controls.RoomComboBox;
import sunforge.controls.Tag;
import sunforge.model.*;

public abstract class AcademicViewContentBase extends StackPane {
    private static final int INITIAL_PREF_WIDTH = 310;
    private static String stylesheet = null;

    protected ObjectProperty<AcademicClass> academicClassProperty;
    protected Subgroup subgroup;

    private VBox composedVBox = new VBox();

    private Label subject = null;
    private RichText type = null;
    private RichText professor = null;
    private HBox classLayout = null;
    private RoomComboBox classComboBox;
    private FlowPane additionalRequirementsLayout = null;
    private Label additionalRequirementsLabel = null;

    private boolean nodesInited = false;

    public AcademicViewContentBase(ObjectProperty<AcademicClass> academicClassProperty, Subgroup subgroup) {
        if (stylesheet == null) {
            stylesheet = AcademicViewContentBase.class.getResource("AcademicViewContentBase.css").toExternalForm();
        }
        composedVBox.getStylesheets().add(stylesheet);
        composedVBox.getStyleClass().add("academic-view");

        composedVBox.setPrefWidth(INITIAL_PREF_WIDTH);
        getChildren().add(composedVBox);

        this.academicClassProperty = academicClassProperty;
        this.subgroup = subgroup;

        render();

        if (SidebarAcademicViewPool.getInstance().getBoundClasses().contains(this.academicClassProperty.get())) {
            makeUndroppable();
        } else {
            makeDroppable();
        }

        this.academicClassProperty.addListener((observableValue, oldAcademicClass, newAcademicClass) -> {
            render();
        });
    }

    /*private void runValidation() {
        composedVBox.getStyleClass().removeAll("error-warning", "error-fatal");
        if (academicClassProperty.getValue() != null) {
            ValidationStorage.getInstance().runAllValidations(this);
        }
    }*/

    private VBox initNodes(AcademicClass academicClass) {
        VBox result = new VBox();

        result.getStylesheets().add(stylesheet);
        result.getStyleClass().add("academic-view");

        result.setPrefWidth(INITIAL_PREF_WIDTH);

        subject = new Label(academicClass.getSubjectName());
        subject.getStyleClass().add("academic-view-subject");
        VBox.setMargin(subject, new Insets(-5, 0, 5, 0));

        type = new RichText("Формат: " + academicClass.getType().toString(), getPrefWidth());
        type.setStyle(
                "-fx-font-family: Roboto;\n" +
                        "-fx-font-size: 16px;");
        //Make Формат: bold
        type.setStyle(0, 6, "-fx-font-weight: bold");
        //-2 because of RichText inconsistency
        VBox.setMargin(type, new Insets(0, 0, 4, -2));

        professor = new RichText("Викладач: " + academicClass.getProfessor().getName(), getPrefWidth());
        professor.getStyleClass().add("academic-view-professor");
        //Make Викладач: bold
        professor.setStyle(
                "-fx-font-family: Roboto;\n" +
                        "-fx-font-size: 16px;");
        professor.setStyle(0, 9, "-fx-font-weight: bold");
        //-2 because of RichText inconsistency
        VBox.setMargin(professor, new Insets(0, 0, 0, -2));

        classLayout = new HBox();
        VBox.setMargin(classLayout, new Insets(10, 0, 0, 0));

        Label classLabel = new Label("Аудиторія:");
        classLabel.getStyleClass().add("academic-view-class-label");
        HBox.setMargin(classLabel, new Insets(0, 5, 0, 0));

        classComboBox = new RoomComboBox();
        classComboBox.setValue(academicClassProperty.getValue().getAssignedRoom());
        classComboBox.valueProperty().addListener((observableValue, oldRoom, newRoom) -> {
            academicClassProperty.get().setAssignedRoom(newRoom);
        });

        //ArtificerComboBox starts with an additional button that appears only
        //while it is active so need to adjust
        HBox.setMargin(classComboBox, new Insets(-ArtificerComboBox.getAdvancedSearchButtonHeight(), 0, 0, 0));

        classLayout.getChildren().addAll(classLabel, classComboBox);

        result.getChildren().addAll(subject, type, professor, classLayout);

        additionalRequirementsLayout = new FlowPane();
        additionalRequirementsLayout.setVgap(5);

        VBox.setMargin(additionalRequirementsLayout, new Insets(8, 0, 0, 0));

        additionalRequirementsLabel = new Label("Дод. умови:");
        additionalRequirementsLabel.getStyleClass().add("academic-view-additional-requirements-label");

        FlowPane.setMargin(additionalRequirementsLabel, new Insets(0, 5, 0, 0));

        if (academicClass.hasAdditionalRequirement()) {
            additionalRequirementsLayout.getChildren().add(additionalRequirementsLabel);
            Tag tagToAdd;
            for (AdditionalRequirement additionalRequirement : academicClass.getAdditionalRequirements()) {
                tagToAdd = new Tag(additionalRequirement.getDescription());
                FlowPane.setMargin(tagToAdd, new Insets(0, 5, 0, 0));
                additionalRequirementsLayout.getChildren().add(tagToAdd);
            }

            result.getChildren().add(additionalRequirementsLayout);
        }

        makeDraggable();

        nodesInited = true;

        return result;
    }

    private void hideRegularNodes() {
        subject.setVisible(false);
        type.setVisible(false);
        professor.setVisible(false);
        classLayout.setVisible(false);
        additionalRequirementsLayout.setVisible(false);
    }

    private void revealRegularNodes() {
        subject.setVisible(true);
        type.setVisible(true);
        professor.setVisible(true);
        classLayout.setVisible(true);
        additionalRequirementsLayout.setVisible(true);
    }

    private void resetDataOnRegularNodes() {
        type.deleteText(8, type.getLength());
        professor.deleteText(10, professor.getLength());

        if (additionalRequirementsLayout.getChildren().size() > 1) {
            additionalRequirementsLayout.getChildren().clear();
            additionalRequirementsLayout.getChildren().add(additionalRequirementsLabel);
        }

        composedVBox.getChildren().remove(additionalRequirementsLayout);
    }

    private void render() {
        if (academicClassProperty.get() == null || academicClassProperty.get().isEmpty()) {
            if (nodesInited) {
                hideRegularNodes();
            }
            composedVBox.getStyleClass().add("academic-view-placeholder");
            makeUndraggable();
        } else {
            composedVBox.getStyleClass().remove("academic-view-placeholder");
            if (!nodesInited) {
                composedVBox.getChildren().setAll(initNodes(academicClassProperty.get()).getChildren());
            } else {
                resetDataOnRegularNodes();
                subject.setText(academicClassProperty.get().getSubjectName());

                type.appendText(academicClassProperty.get().getType().toString());
                type.size(getPrefWidth());

                professor.appendText(academicClassProperty.get().getProfessor().getName());
                professor.size(getPrefWidth());

                classComboBox.setValue(academicClassProperty.getValue().getAssignedRoom());

                if (academicClassProperty.get().hasAdditionalRequirement()) {
                    Tag tagToAdd;
                    for (AdditionalRequirement additionalRequirement : academicClassProperty.get().getAdditionalRequirements()) {
                        tagToAdd = new Tag(additionalRequirement.getDescription());
                        FlowPane.setMargin(tagToAdd, new Insets(0, 5, 0, 0));
                        additionalRequirementsLayout.getChildren().add(tagToAdd);
                    }

                    composedVBox.getChildren().add(additionalRequirementsLayout);
                }

                makeDraggable();
                revealRegularNodes();
            }
        }
    }

    protected abstract void makeDraggable();

    protected abstract void makeDroppable();

    private void makeUndraggable() {
        if (getOnDragDetected() != null) {
            removeEventHandler(MouseEvent.DRAG_DETECTED, getOnDragDetected());
        }
    }

    public void makeUndroppable() {
        if (getOnDragOver() != null) {
            removeEventHandler(DragEvent.DRAG_OVER, getOnDragOver());
        }

        if (getOnDragDropped() != null) {
            removeEventHandler(DragEvent.DRAG_DROPPED, getOnDragDropped());
        }
    }
}