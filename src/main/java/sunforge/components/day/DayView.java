package sunforge.components.day;

import javafx.scene.control.Label;
import javafx.scene.layout.*;
import sunforge.model.Day;
import sunforge.model.AcademicClass;
import sunforge.model.Week;

import java.util.List;

public class DayView extends VBox {
    private static String stylesheet = null;

    public DayView(Week week, Day day, List<AcademicClass> list) {
        if(stylesheet == null){
            stylesheet = getClass().getResource("DayView.css").toExternalForm();
        }
        getStylesheets().add(stylesheet);
        getStyleClass().add("day-view");

        Label dayName = new Label(day.toString());
        dayName.getStyleClass().add("day-name");

        getChildren().addAll(dayName, new DayAcademicViewList(list, week, day));
    }
}