package sunforge.components.day;

import javafx.beans.property.ObjectProperty;
import javafx.scene.SnapshotParameters;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import sunforge.components.AcademicViewContentBase;
import sunforge.components.sidebar.SidebarAcademicViewPool;
import sunforge.model.Day;
import sunforge.model.AcademicClass;
import sunforge.model.AcademicGroup;
import sunforge.model.Subgroup;
import sunforge.model.Week;
import sunforge.utils.LocalDragboard;

public class DayAcademicViewContent extends AcademicViewContentBase {
    private Week week;
    private Day day;
    private int indexInDay;

    public DayAcademicViewContent(ObjectProperty<AcademicClass> academicClassProperty, Subgroup subgroup, Week week, Day day, int indexInDay) {
        super(academicClassProperty, subgroup);
        this.week = week;
        this.day = day;
        this.indexInDay = indexInDay;
    }

    @Override
    protected void makeDraggable() {
        setOnDragDetected(mouseEvent -> {
            Dragboard db = startDragAndDrop(TransferMode.ANY);

            SnapshotParameters snapshotParameters = new SnapshotParameters();
            snapshotParameters.setFill(Color.TRANSPARENT);
            db.setDragView(snapshot(snapshotParameters, null));

            ClipboardContent clipboardContent = new ClipboardContent();
            clipboardContent.putString("AcademicClassDnD");

            db.setContent(clipboardContent);

            LocalDragboard.getInstance().putValue(AcademicClass.class, academicClassProperty.getValue());
            mouseEvent.consume();
        });

        setOnDragDone(dragEvent -> {
            LocalDragboard localDragboard = LocalDragboard.getInstance();
            if (dragEvent.isAccepted() && !localDragboard.hasType(Boolean.class)) {
                //TODO Make sidebar rerender on classes in pool changes.
                if(!academicClassProperty.get().isEmpty()) {
                    SidebarAcademicViewPool.getInstance().getBoundClasses().remove(academicClassProperty.get());
                    AcademicGroup.getCurrentGroup().getUnassignedClasses().remove(academicClassProperty.get());
                }
                academicClassProperty.set(null);
            }

            if (dragEvent.isAccepted() && localDragboard.hasType(String.class)) {
                academicClassProperty.set(null);
            }

            localDragboard.clearAll();

            dragEvent.consume();
        });
    }

    @Override
    protected void makeDroppable() {
        setOnDragOver(dragEvent -> {
            Dragboard db = dragEvent.getDragboard();

            if (LocalDragboard.getInstance().hasType(AcademicClass.class) && academicClassProperty.get() != LocalDragboard.getInstance().getValue(AcademicClass.class) && db.hasString() && db.getString().equals("AcademicClassDnD")) {
                dragEvent.acceptTransferModes(TransferMode.ANY);
            }

            dragEvent.consume();
        });

        setOnDragDropped(dragEvent -> {
            Dragboard db = dragEvent.getDragboard();
            boolean success = false;

            LocalDragboard localDragboard = LocalDragboard.getInstance();

            if (db.hasString() && db.getString().equals("AcademicClassDnD") && localDragboard.hasType(AcademicClass.class)) {

                //Return current class to the pool
                if (academicClassProperty.get() != null && !academicClassProperty.get().isEmpty()) {
                    //TODO NUKE THIS WHEN RERENDER ON POOL CHANGES
                    SidebarAcademicViewPool.getInstance().getBoundClasses().add(academicClassProperty.get());
                    AcademicGroup.getCurrentGroup().getUnassignedClasses().add(academicClassProperty.get());
                }

                AcademicClass value = localDragboard.getValue(AcademicClass.class);

                value.setAssignedWeek(week);
                value.setIndexInDay(indexInDay);
                value.setSubgroup(subgroup);

                academicClassProperty.setValue(value);

                localDragboard.clear(AcademicClass.class);
                success = true;
            }

            dragEvent.setDropCompleted(success);

            dragEvent.consume();
        });
    }
}
