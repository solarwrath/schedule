package sunforge.components.day;

import javafx.beans.property.ObjectProperty;
import sunforge.components.AcademicViewContentBase;
import sunforge.components.AcademicViewWrapperBase;
import sunforge.model.Day;
import sunforge.model.AcademicClass;
import sunforge.model.AcademicGroup;
import sunforge.model.Subgroup;
import sunforge.model.Week;

public class DayAcademicViewWrapper extends AcademicViewWrapperBase {
    private Week week;
    private Day day;
    private int indexInDay;

    public DayAcademicViewWrapper(AcademicClass academicClass, Week week, Day day, int indexInDay) {
        super(academicClass);
        this.indexInDay = indexInDay;
        this.week = week;
        this.day = day;

        listenToChanges(firstAcademicClass);
    }

    public DayAcademicViewWrapper(AcademicClass firstSubgroupClass, AcademicClass secondSubgroupClass, Week week, Day day, int indexInDay) {
        super(firstSubgroupClass, secondSubgroupClass);
        this.indexInDay = indexInDay;
        this.week = week;
        this.day = day;

        listenToChanges(firstAcademicClass);
        listenToChanges(secondAcademicClass);
    }

    @Override
    protected AcademicViewContentBase createAcademicViewContent(ObjectProperty<AcademicClass> academicClassProperty, Subgroup subgroup) {
        return new DayAcademicViewContent(academicClassProperty, subgroup, week, day, indexInDay);
    }

    private void listenToChanges(ObjectProperty<AcademicClass> academicClassProperty) {
        academicClassProperty.addListener((observableValue, oldAcademicClass, newAcademicClass) -> {
            System.out.println("triggered: " + newAcademicClass);
            if(newAcademicClass != null) {
                newAcademicClass.setAssignedWeek(week);
                newAcademicClass.setIndexInDay(indexInDay);

                AcademicGroup
                        .getCurrentGroup()
                        .getAssignedClasses()
                        .get(week)
                        .get(day)
                        .set(
                                getParent()
                                        .getChildrenUnmodifiable().indexOf(this),
                                newAcademicClass
                        );
            }
        });
    }

    public Week getWeek() {
        return week;
    }

    public Day getDay() {
        return day;
    }
}