package sunforge.components.day;

import sunforge.components.AcademicViewListBase;
import sunforge.model.Day;
import sunforge.model.AcademicClass;
import sunforge.model.Subgroup;
import sunforge.model.Week;

import java.util.ArrayList;
import java.util.List;

public class DayAcademicViewList extends AcademicViewListBase {
    private Week week;
    private Day day;

    public DayAcademicViewList(List<AcademicClass> academicClasses, Week week, Day day) {
        super(academicClasses);
        this.week = week;
        this.day = day;

        initializeListener();
    }

    @Override
    protected void render(List<AcademicClass> classes) {
        underlyingVBox.getChildren().clear();

        List<List<AcademicClass>> temp = new ArrayList<>(6);

        for (int i = 0; i < 6; i++) {
            temp.add(new ArrayList<>(List.of(new AcademicClass(i), new AcademicClass(i))));
        }

        classes.forEach(academicClass -> {
            Subgroup subgroup = academicClass.getSubgroup();
            if (subgroup != null) {
                if (subgroup != Subgroup.BOTH) {
                    int index = 0;
                    if (subgroup == Subgroup.SECOND) {
                        index = 1;
                    }

                    temp.get(academicClass.getIndexInDay()).set(index, academicClass);
                } else {
                    temp.get(academicClass.getIndexInDay()).set(0, academicClass);
                    temp.get(academicClass.getIndexInDay()).set(1, academicClass);
                }
            }
        });

        for (int idx = 0; idx < temp.size(); idx++) {
            List<AcademicClass> currentCouple = temp.get(idx);
            AcademicClass firstSubgroupClass = currentCouple.get(0);
            AcademicClass secondSubgroupClass = currentCouple.get(1);
            if (firstSubgroupClass == secondSubgroupClass || (firstSubgroupClass.isEmpty() && secondSubgroupClass.isEmpty())) {
                underlyingVBox.getChildren().add(new DayAcademicViewWrapper(firstSubgroupClass, week, day, idx));
            } else {
                underlyingVBox.getChildren().add(new DayAcademicViewWrapper(firstSubgroupClass, secondSubgroupClass, week, day, idx));
            }
        }
    }

    public Week getWeek() {
        return week;
    }

    public Day getDay() {
        return day;
    }
}