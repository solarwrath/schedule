package sunforge.dao;

import sunforge.model.Day;
import sunforge.model.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ImportService {
    public static void initializeData(){
        Properties properties = new Properties();
        try(InputStream in = new FileInputStream("src/main/resources/db.properties")){
            properties.load(in);
            initializeData(properties);
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    public static final void initializeData(Properties properties) {
        String url = properties.getProperty("url", "jdbc:mysql://194.9.70.244:3306/Univer");
        String username = properties.getProperty("username", "anton");
        String password = properties.getProperty("password", "anton2019");

        try (Connection con = DriverManager.getConnection(url, username, password)) {
            initializeGroups(con);
            initializeRooms(con);
            initializeProfessors(con);

            //Classes shall go last as they depend on Groups, Profs, and Rooms
            initializeClasses(con);

            //Setting up listeners here to avoid writing to db on initialization
            AcademicGroup.getPool().forEach(AcademicGroup::setupListeners);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void initializeGroups(Connection connection) {
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM `groups`;");
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                //TODO Add more parameters
                long ID = rs.getLong("ID");
                String name = rs.getString("name");
                Day excludedDay = Day.valueOf(rs.getString("excluded_day"));

                new AcademicGroupBuilder(ID, name, excludedDay).build();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void initializeRooms(Connection connection) {
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM `rooms`;");
             ResultSet rs = ps.executeQuery();) {
            while (rs.next()) {
                //TODO Add more parameters
                long ID = rs.getLong("ID");
                String name = rs.getString("name");
                boolean isActive = rs.getBoolean("is_blocked");
                int countPlaces = rs.getInt("count_places");
                int countPCs = rs.getInt("count_pcs");

                try {
                    Room.RoomType roomType = Room.RoomType.customValueOf(rs.getString("type"));
                    RoomBuilder builder = new RoomBuilder(ID, name, isActive, roomType);
                    if(countPlaces != 0){
                        builder.setCountPlaces(countPlaces);
                    }
                    if(countPCs != 0){
                        builder.setCountComputers(countPCs);
                    }

                    builder.build();
                }catch (IllegalArgumentException ex){
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void initializeProfessors(Connection connection) {
        try (PreparedStatement ps = connection.prepareStatement("SELECT `ID`, `name` FROM `professors`;");
             ResultSet rs = ps.executeQuery();) {
            while (rs.next()) {
                long ID = rs.getLong("ID");
                String name = rs.getString("name");

                new Professor(ID, name);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void initializeClasses(Connection connection) {
        try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM `classes`;");
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                try {
                    //TODO Add more parameters
                    long ID = rs.getLong("ID");
                    String subjectName = rs.getString("subject");
                    AcademicClassType type = AcademicClassType.valueOf(rs.getString("type"));
                    Professor assignedProfessor = Professor.getProfessorByID(rs.getLong("professor_id"));

                    long roomID = rs.getLong("room_id");
                    Room room = null;
                    if (roomID != 0) {
                        try {
                            room = Room.getRoomById(roomID);
                        } catch (NoSuchRoomException ex) {
                            ex.printStackTrace();
                        }
                    }

                    String additionalRequirementsString = rs.getString("additional_requirements");
                    List<AdditionalRequirement> additionalRequirements = null;

                    if (additionalRequirementsString != null) {
                        additionalRequirements = new ArrayList<>();
                        //TODO Consult about way of storing AdditionalRequirements
                        for (String currentString : additionalRequirementsString.split(";")) {
                            additionalRequirements.add(new AdditionalRequirement(currentString));
                        }
                    }

                    Week assignedWeek = null;
                    String weekString = rs.getString("week");
                    if (weekString != null) {
                        assignedWeek = Week.valueOf(rs.getString("week"));
                    }

                    AcademicClass academicClass = new AcademicClassBuilder(ID, subjectName, type, assignedProfessor)
                            .setWeek(assignedWeek)
                            .setAdditionalRequirements(additionalRequirements)
                            .setRoom(room)
                            .build();

                    String subgroupString = rs.getString("subgroup");
                    if(subgroupString != null){
                        Subgroup subgroupInterpreted = Subgroup.valueOf(subgroupString);
                        academicClass.setSubgroup(subgroupInterpreted);
                    }

                    AcademicGroup assignedGroup = AcademicGroup.getGroupByID(rs.getLong("assigned_group_id"));
                    String place = rs.getString("place");

                    if (place.equals("POOL")) {
                        assignedGroup.getUnassignedClasses().add(academicClass);
                    } else {
                        int indexInDay = rs.getInt("index_in_day");
                        academicClass.setIndexInDay(indexInDay);

                        assignedGroup.getAssignedClasses().get(assignedWeek).get(Day.valueOf(place)).add(indexInDay, academicClass);
                    }
                } catch (NoSuchProfessorException | NoSuchGroupException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}